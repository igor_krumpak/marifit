package si.oranza.marifit.objects;

import si.oranza.marifit.util.OranzaUtility;
import android.util.Log;

public class BodyObject {

	private int id;
	private long time;
	private double weight;
	private double waistVolume;
	private double chestVolume;
	private double bicepsVolume;
	private String frontPicturePath;
	private String sidePicturePath;
	private String backPicturePath;
	private boolean isMetric;

	public static final String IMPERIAL = "Imperial";
	public static final String METRIC = "Metric";
	public static final String UNITS_KEY = "units_type";

	public static final String HEIGHT_KEY = "height";

	public static final String MALE = "Male";
	public static final String FEMALE = "Female";

	public static final String HEIGHT_METRIC_DEFAULT_VALUE = "170";
	public static final String HEIGHT_IMPERIAL_DEFAULT_VALUE = "67";

	private final static double kilogramsPoundsConversion = 2.20462262;
	private final static double centimetersInchesConversion = 2.54;

	// Constructor
	private BodyObject(int id, long time, double weight, double waistVolume,
			double chestVolume, double bicepsVolume, String frontPicturePath,
			String sidePicturePath, String backPicturePath) {
		this.id = id;
		this.weight = weight;
		this.waistVolume = waistVolume;
		this.chestVolume = chestVolume;
		this.bicepsVolume = bicepsVolume;
		this.frontPicturePath = frontPicturePath;
		this.backPicturePath = backPicturePath;
		this.sidePicturePath = sidePicturePath;
		if (time == 0)
			this.time = OranzaUtility.getCurrentTime();
		else
			this.time = time;
	}

	// Static creator
	public static BodyObject createNewBodyObjectForDB(int id, long time,
			double weight, double waistVolume, double chestVolume,
			double bicepsVolume, String frontPicturePath,
			String sidePicturePath, String backPicturePath, String unitType) {

		Log.i("Unit type", unitType);
		if (unitType.equals(IMPERIAL)) {
			Log.i("Weight before conversion", weight + "");
			weight = weight / kilogramsPoundsConversion;
			waistVolume = waistVolume * centimetersInchesConversion;
			chestVolume = chestVolume * centimetersInchesConversion;
			bicepsVolume = bicepsVolume * centimetersInchesConversion;
		}

		return new BodyObject(id, time, weight, waistVolume, chestVolume,
				bicepsVolume, frontPicturePath, sidePicturePath,
				backPicturePath);
	}

	// Static creator
	public static BodyObject createNewBodyObjectForDisplay(int id, long time,
			double weight, double waistVolume, double chestVolume,
			double bicepsVolume, String frontPicturePath,
			String sidePicturePath, String backPicturePath, String unitType) {
		Log.i("Unit type", unitType);
		if (unitType.equals(IMPERIAL)) {
			weight = weight * kilogramsPoundsConversion;
			waistVolume = waistVolume / centimetersInchesConversion;
			chestVolume = chestVolume / centimetersInchesConversion;
			bicepsVolume = bicepsVolume / centimetersInchesConversion;
		}
		return new BodyObject(id, time, weight, waistVolume, chestVolume,
				bicepsVolume, frontPicturePath, sidePicturePath,
				backPicturePath);
	}

	// Getters
	public int getId() {
		return id;
	}

	public long getTime() {
		return time;
	}

	public double getWeight() {
		return OranzaUtility.roundToOnePlace(weight);
	}

	public double getWaistVolume() {
		return OranzaUtility.roundToOnePlace(waistVolume);
	}

	public double getChestVolume() {
		return OranzaUtility.roundToOnePlace(chestVolume);
	}

	public double getBicepsVolume() {
		return OranzaUtility.roundToOnePlace(bicepsVolume);
	}

	public String getFrontPicturePath() {
		return frontPicturePath;
	}

	public String getSidePicturePath() {
		return sidePicturePath;
	}

	public String getBackPicturePath() {
		return backPicturePath;
	}

	public boolean hasValues() {
		if (weight != 0.0 || waistVolume != 0.0 || chestVolume != 0.0
				|| bicepsVolume != 0.0 || frontPicturePath.length() > 0
				|| sidePicturePath.length() > 0 || backPicturePath.length() > 0)
			return true;
		return false;
	}

	// private void changeInchesToCentimeters() {
	// waistVolume = waistVolume * centimetersInchesConversion;
	// chestVolume = chestVolume * centimetersInchesConversion;
	// bicepsVolume = bicepsVolume * centimetersInchesConversion;
	// }
	//
	// private void changeCentimetersToInches() {
	// waistVolume = waistVolume / centimetersInchesConversion;
	// chestVolume = chestVolume / centimetersInchesConversion;
	// bicepsVolume = bicepsVolume / centimetersInchesConversion;
	// }
	//
	// private void changeKilogramsToPounds() {
	// weight = weight * kilogramsPoundsConversion;
	// }
	//
	// private void changePoundsToKilograms() {
	// weight = weight / kilogramsPoundsConversion;
	// }

}
