package si.oranza.marifit.objects;

import android.net.Uri;

public class FitnesObject {

	protected long time;
	protected Uri imageUri;
	protected int id;

	public FitnesObject() {
		super();
	}

	public long getTime() {
		return time;
	}

	public Uri getImageUri() {
		return imageUri;
	}

	public String getImageUriString() {
		return imageUri.toString();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public void setImageUri(Uri imageUri) {
		this.imageUri = imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = Uri.parse(imageUri);
	}

	
}