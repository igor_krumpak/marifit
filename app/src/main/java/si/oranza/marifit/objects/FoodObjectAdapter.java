package si.oranza.marifit.objects;

import java.util.List;

import si.oranza.marifit.dao.ObjectDAO;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FoodObjectAdapter extends BaseAdapter {
	
	private List<FoodObject> foodObjects;
	private Context context;
	
	public FoodObjectAdapter(Context context, List<FoodObject> foodObjects) {
		this.context = context;
		this.foodObjects = foodObjects;
	}
	

	@Override
	public int getCount() {
		return foodObjects.size();
	}

	@Override
	public ObjectDAO getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/*private view holder class*/
    private class FoodObjectHolder {
        TextView comment;
        TextView calories;
        TextView time;
        ImageView foodPicture;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		FoodObjectHolder holder = null;
//		LayoutInflater inflater = (LayoutInflater)
//	            this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//		if(convertView == null) {
//			convertView = inflater.inflate(R.layout.food_picture_adapter, null);
//			holder = new FoodObjectHolder();
//			holder.comment = (TextView)convertView.findViewById(R.id.textView_comment);
//			holder.calories = (TextView)convertView.findViewById(R.id.textView_calories);
//			holder.foodPicture = (ImageView)convertView.findViewById(R.id.imageView_food_picture);
//			holder.time = (TextView)convertView.findViewById(R.id.textView_time);
//			convertView.setTag(holder);
//		} else {
//			holder = (FoodObjectHolder) convertView.getTag();
//		}
//		
//		holder.comment.setText(foodObjects.get(position).getComment());
//		holder.calories.setText(foodObjects.get(position).getCalories() + "");
//	
//		
//		holder.time.setText(OranzaUtility
//				.getTimeInFormatedString(foodObjects.get(position).getTime()));
//		
//		OranzaPictureUtility.loadBitmapOnImageView(
//				foodObjects.get(position).getImageFilePath(), holder.foodPicture, context, 70, 70);
		return convertView;
	}

}
