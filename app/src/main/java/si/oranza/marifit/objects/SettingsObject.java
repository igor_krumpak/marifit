package si.oranza.marifit.objects;

import java.io.Serializable;

/**
 * Created by igor on 25.1.2015.
 */
public class SettingsObject implements Serializable {

    private String sex;
    private int height;

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}

