package si.oranza.marifit.dialog;

import java.util.List;
import si.oranza.marifit.R;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class DialogShowFullSizedPictureSlideShow extends Dialog {

	private ImageView imageViewFullSizedPicture;
	private List<Uri> uriList;
	private Handler hanler;
	private final int speed = 2500;

	private DialogShowFullSizedPictureSlideShow(final Context context,
			final List<Uri> uriList) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		setContentView(R.layout.dialog_full_sized_picture);
		this.uriList = uriList;
		imageViewFullSizedPicture = (ImageView) findViewById(R.id.imageView_full_sized_picture);
		hanler = new Handler();
		displayPicture();
	}

	private void displayPicture() {
		new Thread(new Runnable() {
			public void run() {
				for (final Uri uri : uriList) {
					hanler.post(new Runnable() {
						public void run() {
							ImageLoader.getInstance().displayImage(
									uri.toString(), imageViewFullSizedPicture);
						}
					});
					try {
						Thread.sleep(speed);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				endDialog();	
			}
		}).start();
	}
	
	private void endDialog() {
		this.dismiss();
	}

	@Override
	public void onBackPressed() {
		this.dismiss();
	}

	public static void createDialog(final Context context,
			final List<Uri> uriList) {
		if (uriList != null) {
			new DialogShowFullSizedPictureSlideShow(context, uriList).show();
		}

	}
}
