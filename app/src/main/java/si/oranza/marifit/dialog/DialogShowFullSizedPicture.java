package si.oranza.marifit.dialog;

import si.oranza.marifit.R;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class DialogShowFullSizedPicture extends Dialog {

	private ImageView imageViewFullSizedPicture;
	private Uri uri;

	private DialogShowFullSizedPicture(final Context context, final Uri uri) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		setContentView(R.layout.dialog_full_sized_picture);
		this.uri = uri;
		imageViewFullSizedPicture = (ImageView) findViewById(R.id.imageView_full_sized_picture);
		displayPicture();
	}

	private void displayPicture() {
		ImageLoader.getInstance().displayImage(uri.toString(),
				imageViewFullSizedPicture);
	}

	@Override
	public void onBackPressed() {
		this.dismiss();
	}

	public static void createDialog(final Context context, final Uri uri) {
		if (uri != null) {
			new DialogShowFullSizedPicture(context, uri).show();
		}

	}
}
