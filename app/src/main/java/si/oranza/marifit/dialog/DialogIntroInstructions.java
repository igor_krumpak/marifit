package si.oranza.marifit.dialog;

import si.oranza.marifit.R;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class DialogIntroInstructions extends Dialog implements View.OnClickListener{
	
	private Typeface typeFace;
	private TextView textView;
    private ImageButton buttonContinue;
	
	
	
	public DialogIntroInstructions(Context context, OnDismissListener onDismissListener) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		setContentView(R.layout.dialog_intro_instructions);
		setOnDismissListener(onDismissListener);
		typeFace = Typeface.createFromAsset(context.getAssets(),
				"fonts/KGBeStillAndKnow.ttf");
		textView = (TextView)findViewById(R.id.textView_intro_instructions);
		textView.setTypeface(typeFace);
        buttonContinue = (ImageButton)findViewById(R.id.image_button_continue);
        buttonContinue.setOnClickListener(this);
	}

	public static void createDialog(final Context context, final OnDismissListener onDismissListener) {
			new DialogIntroInstructions(context, onDismissListener).show();

	}

    @Override
    public void onClick(View v) {
        this.dismiss();
    }
}
