package si.oranza.marifit.dialog;

import si.oranza.marifit.R;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

public class DialogSeeFoodPicturesInstructions extends Dialog {
	
	private Typeface typeFace;
	private TextView textView;
	
	
	
	public DialogSeeFoodPicturesInstructions(Context context, OnDismissListener onDismissListener) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		setContentView(R.layout.dialog_intro_instructions_see_food_pictures);
		setOnDismissListener(onDismissListener);
		typeFace = Typeface.createFromAsset(context.getAssets(),
				"fonts/KGBeStillAndKnow.ttf");
		textView = (TextView)findViewById(R.id.textView_intro_instructions);
		textView.setTypeface(typeFace);	
	}

	public static void createDialog(final Context context, final OnDismissListener onDismissListener) {
			new DialogSeeFoodPicturesInstructions(context, onDismissListener).show();
		

	}
}
