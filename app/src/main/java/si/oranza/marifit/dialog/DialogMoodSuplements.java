package si.oranza.marifit.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import si.oranza.marifit.R;

public class DialogMoodSuplements extends Dialog implements View.OnClickListener {

    private ImageView currentIcon;
    private TextView currentMood;
    private ImageView productPicture;
    private TextView productDescription;
    private TextView productEfect;
    private String productLink;
    private Button moreInfo;
    private Button shareWithFriends;
    private Context context;
    private Typeface typeface;

    public DialogMoodSuplements(final Context context, int currentIconResource, String currentMoodText, int productPictureResource, int productPictureBackgroundResource, String productDescriptionText, String productEfectText, String productLink) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        setContentView(R.layout.dialog_mood_suplements);
        this.context = context;
        typeface = Typeface.createFromAsset(context.getAssets(),
                "fonts/KGBeStillAndKnow.ttf");
        currentIcon = (ImageView) findViewById(R.id.imageView_current_icon);
        currentMood = (TextView) findViewById(R.id.textView_current_mood);
        currentMood.setTypeface(typeface);
        productPicture = (ImageView) findViewById(R.id.imageView_product_picture);
        productDescription = (TextView) findViewById(R.id.textView_product_description);
        productEfect = (TextView) findViewById(R.id.textView_product_efect);
        productEfect.setTypeface(typeface);
        currentIcon.setImageResource(currentIconResource);
        currentMood.setText(currentMoodText);
        productPicture.setImageResource(productPictureResource);
        productPicture.setBackgroundResource(productPictureBackgroundResource);
        productDescription.setText(productDescriptionText);
        productDescription.setTypeface(typeface);
        productEfect.setText(productEfectText);
        this.productLink = productLink;
        moreInfo = (Button)findViewById(R.id.button_more_info);
        moreInfo.setOnClickListener(this);
        moreInfo.setTypeface(typeface);
        shareWithFriends = (Button)findViewById(R.id.button_share_with_friends);
        shareWithFriends.setOnClickListener(this);
        shareWithFriends.setTypeface(typeface);
        //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
        //startActivity(browserIntent);
    }

    public static void createDialog(final Context context, int currentIconResource, String currentMoodText, int productPictureResource, int productPictureBackgroundResource, String productDescriptionText, String productEfectText, String productLink) {
        new DialogMoodSuplements(context, currentIconResource, currentMoodText, productPictureResource, productPictureBackgroundResource, productDescriptionText, productEfectText, productLink).show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_more_info:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(productLink));
                context.startActivity(browserIntent);
                break;
            case R.id.button_share_with_friends:
                String shareIt = context.getResources().getString(R.string.shareProduct);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, productLink);
                context.startActivity(Intent.createChooser(share, shareIt));
                break;
        }
    }
}
