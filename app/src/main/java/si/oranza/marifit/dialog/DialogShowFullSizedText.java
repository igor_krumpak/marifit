package si.oranza.marifit.dialog;

import si.oranza.marifit.R;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

public class DialogShowFullSizedText extends Dialog {

	private TextView textViewFullSizedText;;

	private DialogShowFullSizedText(final Context context, final String text) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		setContentView(R.layout.dialog_full_sized_text);
		Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/KGBeStillAndKnow.ttf");
		textViewFullSizedText = (TextView) findViewById(R.id.textView_full_sized_text);
		textViewFullSizedText.setTypeface(typeFace);
		textViewFullSizedText.setText(text);
	}

	@Override
	public void onBackPressed() {
		this.dismiss();
	}

	public static void createDialog(final Context context, final String text) {
		if (text.length() != 0) {
			new DialogShowFullSizedText(context, text).show();
		}

	}
}
