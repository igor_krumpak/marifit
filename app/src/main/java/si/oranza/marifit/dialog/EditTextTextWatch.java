package si.oranza.marifit.dialog;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class EditTextTextWatch implements TextWatcher {

	private String textToAdd;
	private EditText editText;

	public EditTextTextWatch(String textToAdd, EditText editText) {
		this.textToAdd = textToAdd;
		this.editText = editText;
		editText.addTextChangedListener(this);
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		//editText.setText(textToAdd + s);

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		editText.setText(textToAdd);

	}

}
