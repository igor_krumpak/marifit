package si.oranza.marifit.activity;

import java.util.ArrayList;

import si.iitech.library.activity.IITechActivity;
import si.oranza.marifit.R;
import si.oranza.marifit.adapter.FoodAdapter;
import si.oranza.marifit.dao.FoodDAO;
import si.oranza.marifit.dialog.DialogShowFullSizedPicture;
import si.oranza.marifit.dialog.DialogShowFullSizedText;
import si.oranza.marifit.service.FoodService;
import android.app.ActionBar;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

public class SeeFoodPicturesActivity extends IITechActivity implements
		OnItemClickListener, OnItemLongClickListener, OnMenuItemClickListener {

	private ListView listView;
	private FoodService service;
	private FoodAdapter adapter;
	private long itemToBeDeleted;
	private TextView textViewViewName;

	private String alertDialogMessage;
	private String alertDialogYesMessage;
	private String alertDialogNoMessage;

	private ImageButton imageButtonDateSort;

	private Typeface typeFace;

	private FoodDAO foodEntry;

	private final int ALL_ENTRIES = 1;
	private final int ALL_ENTRIES_THIS_DAY = 2;
	private final int ALL_ENTRIES_THIS_WEEK = 3;
	private final int ALL_ENTRIES_THIS_MONTH = 4;

	private int SELECTED_ENTRIES = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_see_food_pictures);
		initGraphicComponents();
		initAdapter();
	}

	private void initAdapter() {
		ArrayList<FoodDAO> entries = service.returnAllFoodEntries();
		if (0 == entries.size()) {
			createToastMessage(getResources().getString(R.string.no_food_entry));
			this.finish();
			//startNewActivity(TakeFoodPictureActivity.class);
		} else {
			adapter = new FoodAdapter(this, entries);
			listView.setAdapter(adapter);
			listView.setOnItemClickListener(this);
			listView.setOnItemLongClickListener(this);
		}

	}


	@Override
	protected void initGraphicComponents() {
		typeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KGBeStillAndKnow.ttf");
		listView = (ListView) findViewById(R.id.listView_food_pictures);
		service = new FoodService(this);
		alertDialogMessage = getResources().getString(
				R.string.alert_dialog_message_delete);
		alertDialogYesMessage = getResources().getString(
				R.string.alert_dialog_yes_message);
		alertDialogNoMessage = getResources().getString(
				R.string.alert_dialog_no_message);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		log(arg2 + " " + arg3);
		foodEntry = service.returnFoodEntry((int) arg3);
		PopupMenu popup = new PopupMenu(this, arg1);
		popup.inflate(R.menu.see_food_pictures_adapter_options);
		popup.setOnMenuItemClickListener(this);
		popup.show();

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long id) {
		itemToBeDeleted = id;
		createYesNoAlertDialog(alertDialogMessage, alertDialogYesMessage, alertDialogNoMessage, dialogClickListener);
//		createYesNoAlertDialog(alertDialogMessage, alertDialogYesMessage,
//				alertDialogNoMessage, dialogClickListener, typeFace);
		return false;
	}

	private void deleteFoodEntry(int id) {
		service.deleteFoodEntry(id);
		ArrayList<FoodDAO> entries = service.returnAllFoodEntries();
		refreshAdapter(entries);

	}

	private void refreshAdapter(ArrayList<FoodDAO> list) {
		adapter.setList(list);
		adapter.notifyDataSetChanged();
	}

	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				deleteFoodEntry((int) itemToBeDeleted);
				break;
			}
		}

		private void deleteFoodEntry(int id) {
			service.deleteFoodEntry(id);

			switch (SELECTED_ENTRIES) {
			case ALL_ENTRIES:
				refreshAdapter(service.returnAllFoodEntries());
				break;
			case ALL_ENTRIES_THIS_DAY:
				ArrayList<FoodDAO> listThisDay = service
						.returnAllFoodEntriesThisDay();
				if (0 == listThisDay.size()) {
					refreshAdapter(service.returnAllFoodEntries());
				} else {
					refreshAdapter(service.returnAllFoodEntriesThisDay());
				}
				break;
			case ALL_ENTRIES_THIS_WEEK:
				ArrayList<FoodDAO> listThisWeek = service
						.returnAllFoodEntriesThisWeek();
				if (0 == listThisWeek.size()) {
					refreshAdapter(service.returnAllFoodEntries());
				} else {
					refreshAdapter(service.returnAllFoodEntriesThisDay());
				}
				break;
			case ALL_ENTRIES_THIS_MONTH:
				ArrayList<FoodDAO> listThisMonth = service
						.returnAllFoodEntriesThisMonth();
				if (0 == listThisMonth.size()) {
					refreshAdapter(service.returnAllFoodEntries());
				} else {
					refreshAdapter(service.returnAllFoodEntriesThisDay());
				}
				break;
			default:
				break;
			}

			// ArrayList<FoodDAO> entries = service.returnAllFoodEntries();
			// adapter.setList(entries);
			// adapter.notifyDataSetChanged();

		}
	};

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.adapter_show_image:
			DialogShowFullSizedPicture.createDialog(this, foodEntry.getUri());
			return true;
		case R.id.adapter_show_text:
			DialogShowFullSizedText.createDialog(this, foodEntry.getComment());
			return true;
		case R.id.adapter_delete_entry:
			deleteFoodEntry(foodEntry.getId());
			return true;
		case R.id.adapter_show_all:
			SELECTED_ENTRIES = ALL_ENTRIES;
			refreshAdapter(service.returnAllFoodEntries());
			return true;
		case R.id.adapter_show_this_day:
			SELECTED_ENTRIES = ALL_ENTRIES_THIS_DAY;
			refreshAdapter(service.returnAllFoodEntriesThisDay());
			return true;
		case R.id.adapter_show_last_week:
			SELECTED_ENTRIES = ALL_ENTRIES_THIS_WEEK;
			refreshAdapter(service.returnAllFoodEntriesThisWeek());
			return true;
		case R.id.adapter_show_last_month:
			SELECTED_ENTRIES = ALL_ENTRIES_THIS_MONTH;
			refreshAdapter(service.returnAllFoodEntriesThisMonth());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void home(View v) {
		this.finish();
	}

	public void dateSort(View v) {
		PopupMenu popup = new PopupMenu(this, imageButtonDateSort);
		popup.inflate(R.menu.see_food_date_sort_options);
		popup.setOnMenuItemClickListener(this);
		popup.show();

	}

}
