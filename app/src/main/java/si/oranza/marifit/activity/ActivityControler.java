package si.oranza.marifit.activity;

import si.oranza.marifit.settings.SettingsMetricActivity;
import android.app.Activity;
import android.content.Intent;

public class ActivityControler {

	public static void startNewActivity(Activity activity,
			@SuppressWarnings("rawtypes") Class target) {
		Intent intent = new Intent(activity, target);
		activity.startActivity(intent);
	}

	public static void startTakeFoodPictureActivity(Activity activity) {
		startNewActivity(activity, TakeFoodPictureActivity.class);
	}

	public static void startSeeFoodPictureActivity(Activity activity) {
		startNewActivity(activity, SeeFoodPicturesActivity.class);
	}

	public static void startSeeBodyPartsActivity(Activity activity) {
		startNewActivity(activity, SeeBodyPartsPicturesActivity.class);
	}

	public static void startTakeBodyPictureActivity(Activity activity) {
		startNewActivity(activity, TakeBodyPicturesActivity.class);
	}

	public static void startSettingsActivity(Activity activity) {
		startNewActivity(activity, SettingsMetricActivity.class);
	}
}
