package si.oranza.marifit.activity;

import si.iitech.library.activity.IITechActivity;
import si.oranza.marifit.R;
import si.oranza.marifit.dialog.DialogIntroInstructions;
import si.oranza.marifit.dialog.DialogIntroInstructionsBeforeSettings;
import si.oranza.marifit.dialog.DialogSeeBodyPicturesInstructions;
import si.oranza.marifit.dialog.DialogSeeFoodPicturesInstructions;
import si.oranza.marifit.dialog.DialogTakeBodyPicturesInstructions;
import si.oranza.marifit.dialog.DialogTakeFoodPictureInstructions;
import si.oranza.marifit.settings.SettingsImperialActivity;
import si.oranza.marifit.settings.SettingsMetricActivity;
import android.app.ActionBar;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;


public class IntroActivity extends IITechActivity implements OnClickListener,
		OnDismissListener {

	private final String FIRST_RUN = "FIRST_RUN";
	private final String UNITS = "UNITS";
	private final String UNITS_METRIC = "UNITS_METRIC";
	private final String UNITS_IMPERIAL = "UNITS_IMPERIAL";

	private final String FIRST_RUN_TAKE_FOOD_PICTURE = "TAKE_FOOD_PICTURE";
	private final String FIRST_RUN_SEE_FOOD_PICTURES = "SEE_FOOD_PICTUREs";
	private final String FIRST_RUN_TAKE_BODY_PICTURES = "TAKE_BODY_PICTURES";
	private final String FIRST_RUN_SEE_BODY_PICTURES = "SEE_BODY_PICTURES";

    private TextView textViewHowDoYouFeelToday;
    private TextView textViewTakeFoodPicture;
    private TextView textViewViewFoodPictures;
    private TextView textViewViewBodyPictures;
    private TextView textViewTakeBodyPictures;





	private Typeface typeface;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);
		initGraphicComponents();
		checkFirstRun();
	}

	private void checkFirstRun() {
		if (!retriveFromSharedPreferencesBoolean(FIRST_RUN, false)) {
			saveInSharedPreferences(FIRST_RUN, true);

            DialogIntroInstructions.createDialog(this, this);
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	public void startSettings(View v) {
        this.startNewActivity(SettingsActivity.class);
	}

	public void startTakeFoodPicture(View v) {
		if (!retriveFromSharedPreferencesBoolean(FIRST_RUN_TAKE_FOOD_PICTURE, false)) {
			saveInSharedPreferences(FIRST_RUN_TAKE_FOOD_PICTURE, true);
            DialogTakeFoodPictureInstructions.createDialog(this, this);
        } else {
			this.startNewActivity(TakeFoodPictureActivity.class);
		}

	}

    public void startHowYouFeel(View v) {
        this.startNewActivity(MoodActivity.class);
    }

	public void startViewFoodPictures(View v) {
        this.startNewActivity(SeeFoodPicturesActivity.class);
	}

	public void startTakeBodyPicture(View v) {
		// this.startNewActivity(TakeBodyPicturesActivity.class);
		if (!retriveFromSharedPreferencesBoolean(FIRST_RUN_TAKE_BODY_PICTURES, false)) {
			saveInSharedPreferences(FIRST_RUN_TAKE_BODY_PICTURES, true);
            DialogTakeBodyPicturesInstructions.createDialog(this, this);
		} else {
			this.startNewActivity(TakeBodyPicturesActivity.class);
		}
	}

	public void startViewBodyPictures(View v) {
        this.startNewActivity(SeeBodyPartsPicturesActivity.class);
	}

	@Override
	protected void initGraphicComponents() {
		typeface = Typeface.createFromAsset(getAssets(),
				"fonts/KGBeStillAndKnow.ttf");
        textViewTakeBodyPictures = (TextView) findViewById(R.id.textView_take_body_pictures);
        textViewTakeBodyPictures.setTypeface(typeface);
        textViewHowDoYouFeelToday = (TextView) findViewById(R.id.textView_how_do_you_feel_today);
        textViewHowDoYouFeelToday.setTypeface(typeface);
        textViewViewBodyPictures = (TextView)findViewById(R.id.textView_view_body_pictures);
        textViewViewBodyPictures.setTypeface(typeface);
        textViewViewFoodPictures = (TextView)findViewById(R.id.textView_view_food_pictures);
        textViewViewFoodPictures.setTypeface(typeface);
        textViewTakeFoodPicture = (TextView)findViewById(R.id.textView_take_food_pictures);
        textViewTakeFoodPicture.setTypeface(typeface);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case DialogInterface.BUTTON_NEGATIVE:
			saveInSharedPreferences(UNITS, UNITS_IMPERIAL);
			DialogIntroInstructionsBeforeSettings.createDialog(this, this);
			break;
		case DialogInterface.BUTTON_POSITIVE:
			saveInSharedPreferences(UNITS, UNITS_METRIC);
			DialogIntroInstructionsBeforeSettings.createDialog(this, this);
			break;
		}

	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (dialog instanceof DialogIntroInstructions) {
            this.startNewActivity(SettingsActivity.class);
		}
		if (dialog instanceof DialogTakeFoodPictureInstructions) {
			this.startNewActivity(TakeFoodPictureActivity.class);
		}
		if (dialog instanceof DialogSeeFoodPicturesInstructions) {
			this.startNewActivity(SeeFoodPicturesActivity.class);
		}
		if (dialog instanceof DialogTakeBodyPicturesInstructions) {
			this.startNewActivity(TakeBodyPicturesActivity.class);
		}
		if (dialog instanceof DialogSeeBodyPicturesInstructions) {
			this.startNewActivity(SeeBodyPartsPicturesActivity.class);
		}

	}
}
