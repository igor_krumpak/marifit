package si.oranza.marifit.activity;


import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import si.oranza.marifit.R;
import si.oranza.marifit.dialog.DialogMoodSuplements;

public class MoodActivity extends Activity implements View.OnClickListener, View.OnTouchListener {

    private Button moodFantastic;
    private Button moodHappy;
    private Button moodGood;
    private Button moodSoSo;
    private Button moodDontKnow;
    private Button moodBad;
    private Button moodTired;
    private Button moodSad;
    private Button moodAngry;

    private TextView textView1;
    private TextView textView2;
    private Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_mood);
        initGraphicComponents();
    }

    private void initGraphicComponents() {
        typeface = Typeface.createFromAsset(getAssets(),
                "fonts/KGBeStillAndKnow.ttf");
        moodFantastic = (Button) findViewById(R.id.moodFantastic);
        moodFantastic.setTypeface(typeface);
        moodFantastic.setOnClickListener(this);
        moodFantastic.setOnTouchListener(this);
        moodHappy = (Button) findViewById(R.id.moodHappy);
        moodHappy.setTypeface(typeface);
        moodHappy.setOnClickListener(this);
        moodHappy.setOnTouchListener(this);
        moodGood = (Button) findViewById(R.id.moodGood);
        moodGood.setTypeface(typeface);
        moodGood.setOnClickListener(this);
        moodGood.setOnTouchListener(this);
        moodSoSo = (Button) findViewById(R.id.moodSoSo);
        moodSoSo.setTypeface(typeface);
        moodSoSo.setOnClickListener(this);
        moodSoSo.setOnTouchListener(this);
        moodDontKnow = (Button) findViewById(R.id.moodDontKnow);
        moodDontKnow.setTypeface(typeface);
        moodDontKnow.setOnClickListener(this);
        moodDontKnow.setOnTouchListener(this);
        moodBad = (Button) findViewById(R.id.moodBad);
        moodBad.setTypeface(typeface);
        moodBad.setOnClickListener(this);
        moodBad.setOnTouchListener(this);
        moodTired = (Button) findViewById(R.id.moodTired);
        moodTired.setTypeface(typeface);
        moodTired.setOnClickListener(this);
        moodTired.setOnTouchListener(this);
        moodSad = (Button) findViewById(R.id.moodSad);
        moodSad.setTypeface(typeface);
        moodSad.setOnClickListener(this);
        moodSad.setOnTouchListener(this);
        moodAngry = (Button) findViewById(R.id.moodAngry);
        moodAngry.setTypeface(typeface);
        moodAngry.setOnTouchListener(this);
        moodAngry.setOnClickListener(this);
        textView1 = (TextView) findViewById(R.id.textView6);
        textView1.setTypeface(typeface);
        textView2 = (TextView) findViewById(R.id.textView7);
        textView2.setTypeface(typeface);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.moodHappy:
                DialogMoodSuplements.createDialog(this, R.drawable.happy_icon_orange, getResources().getString(R.string.moodHappy1), R.drawable.magnezij_item, R.drawable.magnezij_background,getResources().getString(R.string.moodHappy2), getResources().getString(R.string.moodHappy3), getResources().getString(R.string.magnezij300Link));
                break;
            case R.id.moodFantastic:
                DialogMoodSuplements.createDialog(this, R.drawable.fantastic_icon_orange, getResources().getString(R.string.moodFantastic1), R.drawable.kalcij_item, R.drawable.kalcij_background, getResources().getString(R.string.moodFantastic2), getResources().getString(R.string.moodFantastic3), getResources().getString(R.string.kalcij500Link));
                break;
            case R.id.moodGood:
                DialogMoodSuplements.createDialog(this, R.drawable.good_icon_orange, getResources().getString(R.string.moodGood1), R.drawable.kalcij_item, R.drawable.kalcij_background, getResources().getString(R.string.moodGood2), getResources().getString(R.string.moodGood3), getResources().getString(R.string.kalcij500Link));
                break;
            case R.id.moodSoSo:
                DialogMoodSuplements.createDialog(this, R.drawable.so_so_icon_orange, getResources().getString(R.string.moodSoSo1), R.drawable.omegavit_item, R.drawable.omegavit_background, getResources().getString(R.string.moodSoSo2), getResources().getString(R.string.moodSoSo3), getResources().getString(R.string.omegavitLink));
                break;
            case R.id.moodDontKnow:
                DialogMoodSuplements.createDialog(this, R.drawable.dont_know_icon_orange, getResources().getString(R.string.moodDontKnow1), R.drawable.multivitamin_item, R.drawable.multivitamin_background ,getResources().getString(R.string.moodDontKnow2), getResources().getString(R.string.moodDontKnow3), getResources().getString(R.string.multivitLink));
                break;
            case R.id.moodBad:
                DialogMoodSuplements.createDialog(this, R.drawable.bad_icon_orange, getResources().getString(R.string.moodBad1), R.drawable.multivitamin_item, R.drawable.multivitamin_background, getResources().getString(R.string.moodBad2), getResources().getString(R.string.moodBad3), getResources().getString(R.string.multivitLink));
                break;
            case R.id.moodTired:
                DialogMoodSuplements.createDialog(this, R.drawable.tired_icon_orange, getResources().getString(R.string.moodTired1), R.drawable.koencim_item, R.drawable.koencim_background, getResources().getString(R.string.moodTired2), getResources().getString(R.string.moodTired3), getResources().getString(R.string.koencimQ10Link));
                break;
            case R.id.moodSad:
                DialogMoodSuplements.createDialog(this, R.drawable.sad_icon_orange, getResources().getString(R.string.moodSad1), R.drawable.omegavit_item, R.drawable.omegavit_background, getResources().getString(R.string.moodSad2), getResources().getString(R.string.moodSad3), getResources().getString(R.string.omegavitLink));
                break;
            case R.id.moodAngry:
                DialogMoodSuplements.createDialog(this, R.drawable.angry_icon_orange, getResources().getString(R.string.moodAngry1), R.drawable.magnezij_item, R.drawable.magnezij_background, getResources().getString(R.string.moodAngry2), getResources().getString(R.string.moodAngry3), getResources().getString(R.string.magnezij300Link));
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (v.getId()) {
                    case R.id.moodHappy:
                        moodGood.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodFantastic.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSoSo.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodDontKnow.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodBad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodTired.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodAngry.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        break;
                    case R.id.moodFantastic:
                        moodHappy.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodGood.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSoSo.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodDontKnow.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodBad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodTired.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodAngry.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        break;
                    case R.id.moodGood:
                        moodHappy.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodFantastic.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSoSo.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodDontKnow.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodBad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodTired.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodAngry.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        break;
                    case R.id.moodSoSo:
                        moodHappy.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodFantastic.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodGood.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodDontKnow.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodBad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodTired.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodAngry.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        break;
                    case R.id.moodDontKnow:
                        moodHappy.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodFantastic.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSoSo.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodGood.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodBad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodTired.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodAngry.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        break;
                    case R.id.moodBad:
                        moodHappy.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodFantastic.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSoSo.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodDontKnow.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodGood.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodTired.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodAngry.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        break;
                    case R.id.moodTired:
                        moodHappy.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodFantastic.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSoSo.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodDontKnow.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodBad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodGood.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodAngry.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        break;
                    case R.id.moodSad:
                        moodHappy.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodFantastic.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSoSo.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodDontKnow.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodBad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodTired.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodGood.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodAngry.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        break;
                    case R.id.moodAngry:
                        moodHappy.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodFantastic.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSoSo.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodDontKnow.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodBad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodTired.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodSad.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        moodGood.setBackgroundColor(getResources().getColor(R.color.mood_unselected));
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                moodHappy.setBackgroundColor(getResources().getColor(R.color.mood_happy));
                moodGood.setBackgroundColor(getResources().getColor(R.color.mood_good));
                moodFantastic.setBackgroundColor(getResources().getColor(R.color.mood_fantastic));
                moodSoSo.setBackgroundColor(getResources().getColor(R.color.mood_so_so));
                moodDontKnow.setBackgroundColor(getResources().getColor(R.color.mood_dont_know));
                moodBad.setBackgroundColor(getResources().getColor(R.color.mood_bad));
                moodTired.setBackgroundColor(getResources().getColor(R.color.mood_tired));
                moodSad.setBackgroundColor(getResources().getColor(R.color.mood_sad));
                moodAngry.setBackgroundColor(getResources().getColor(R.color.mood_angry));
                break;
        }
        return false;
    }
}
