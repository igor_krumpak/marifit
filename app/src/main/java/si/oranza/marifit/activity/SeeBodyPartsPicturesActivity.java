package si.oranza.marifit.activity;

import java.util.List;

import si.iitech.library.activity.IITechActivity;
import si.iitech.library.util.IITechUtil;
import si.iitech.library.util.Serialize;
import si.oranza.marifit.R;
import si.oranza.marifit.dao.BodyDAO;
import si.oranza.marifit.dialog.DialogShowFullSizedPicture;
import si.oranza.marifit.dialog.DialogShowFullSizedPictureSlideShow;
import si.oranza.marifit.objects.SettingsObject;
import si.oranza.marifit.service.BodyService;
import si.oranza.marifit.util.OranzaFitnesUtility;
import android.app.ActionBar;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class SeeBodyPartsPicturesActivity extends IITechActivity implements
		OnMenuItemClickListener {

	private TextView textViewDate;

	private TextView textViewWeight;
	private TextView textViewWaist;
	private TextView textViewBiceps;
	private TextView textViewChest;
	private TextView textViewBMI;

	private ImageView imageViewFrontPicture;
	private ImageView imageViewSidePicture;
	private ImageView imageViewBackPicture;

	private ImageButton imageButtonNextPage;
	private ImageButton imageButtonPreviusPage;

	private BodyService service;
	private List<BodyDAO> entries;
	private BodyDAO entry;

	private int currentPosition;
	private int endPosition;

	private String gender;
	private double height;

	private Typeface typeFace;

	private String textViewWeightBonusText;
	private String textViewWaistBonusText;
	private String textViewChestBonusText;
	private String textViewBicepsBonusText;
	private String textViewBMIBonusText;

	private final String UNITS = "UNITS";
	private final String UNITS_METRIC = "UNITS_METRIC";

	private DisplayImageOptions optionsFrontPicture;
	private DisplayImageOptions optionsSidePicture;
	private DisplayImageOptions optionsBackPicture;

	private ImageButton imageButtonSlideShow;
    private Button buttonDeleteSaveBodyEntry;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_see_body_pictures);
		initBonusText();
		initGraphicComponents();
		initImageLoaderDisplayOptionsFront();
		initImageLoaderDisplayOptionsSide();
		initImageLoaderDisplayOptionsBack();
		getSettings();
		getEntries();
	}

	private void initImageLoaderDisplayOptionsFront() {
		optionsFrontPicture = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(
						R.drawable.see_body_front_picture_background)
				.displayer(new FadeInBitmapDisplayer(500))
				.showImageOnFail(R.drawable.see_body_front_picture_background)
				.build();
	}

	private void initImageLoaderDisplayOptionsSide() {
		optionsSidePicture = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(
						R.drawable.see_body_side_picture_background)
				.displayer(new FadeInBitmapDisplayer(500))
				.showImageOnFail(R.drawable.see_body_side_picture_background)
				.build();
	}

	private void initImageLoaderDisplayOptionsBack() {
		optionsBackPicture = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(
						R.drawable.see_body_back_picture_background)
				.displayer(new FadeInBitmapDisplayer(500))
				.showImageOnFail(R.drawable.see_body_back_picture_background)
				.build();
	}

	private void initBonusText() {

        textViewWeightBonusText = getResources().getString(
                R.string.bonusTextWeightMetric);
        textViewWaistBonusText = getResources().getString(
                R.string.bonusTextWaistMetric);
        textViewChestBonusText = getResources().getString(
                R.string.bonusTextChestMetric);
        textViewBicepsBonusText = getResources().getString(
                R.string.bonusTextBicepsMetric);
		textViewBMIBonusText = "BMI: ";
	}

	private void getSettings() {
        SettingsObject temp = Serialize.deserialize(this, SettingsObject.class, "settings1", "set");
        if(temp == null) {
            gender = "Male";
            height = 170;
        } else {
            gender = temp.getSex();
            height = temp.getHeight();
        }
	}

	private void getEntries() {
		entries = service.getAllEntries();
		if (0 == entries.size()) {
			createToastMessage(getResources().getString(R.string.no_body_entry));
			this.finish();
		} else {
			currentPosition = 0;
			endPosition = entries.size() - 1;
			displayEntry();
		}
	}

	private void showBodyValueOnTextView(TextView textView, int value,
			String defaultValue, String bonusText) {
		if (0 != value) {
			textView.setText(bonusText + " " + value);
		} else {
			textView.setText(bonusText + " " + defaultValue);
		}
	}

	private void showBodyValueOnTextView(TextView textView, double value,
			String defaultValue, String bonusText) {
		if (0 != value) {
			textView.setText(bonusText + " " + value);
		} else {
			textView.setText(bonusText + " " + defaultValue);
		}
	}

	private void displayEntry() {
		entry = entries.get(currentPosition);
		log(entry.toString());
		textViewDate.setText(IITechUtil.formatTimeToDMY(entry.getTime()));
		showBodyValueOnTextView(textViewWeight, entry.getWeight(), "/",
				textViewWeightBonusText);
		showBodyValueOnTextView(textViewWaist, entry.getWaist(), "/",
				textViewWaistBonusText);
		showBodyValueOnTextView(textViewBiceps, entry.getBiceps(), "/",
				textViewBicepsBonusText);
		showBodyValueOnTextView(textViewChest, entry.getChest(), "/",
				textViewChestBonusText);

		double BMI = 0;
		String unitType = retriveFromSharedPreferences(UNITS, UNITS_METRIC);
		if (unitType.equalsIgnoreCase(UNITS_METRIC)) {
			BMI = OranzaFitnesUtility.calculateBMI(entry.getWeight(), height);
		} else {
			BMI = OranzaFitnesUtility.calculateBMIImperial(entry.getWeight(),
					height);
		}

		displayBMI(BMI);

		ImageLoader.getInstance().displayImage(
				entry.getFrontPictureUri().toString(), imageViewFrontPicture,
				optionsFrontPicture);

		ImageLoader.getInstance().displayImage(
				entry.getSidePictureUri().toString(), imageViewSidePicture,
				optionsSidePicture);
		ImageLoader.getInstance().displayImage(
				entry.getBackPictureUri().toString(), imageViewBackPicture,
				optionsBackPicture);
		hideAndDisplayButtons();
	}

	private static final double BMI_MALE_YELLOW_BORDER = 20.7;
	private static final double BMI_MALE_GREEN_BORDER = 26.4;
	private static final double BMI_MALE_ORANGE_BORDER = 31.1;
	private static final double BMI_FEMALE_YELLOW_BORDER = 19.1;
	private static final double BMI_FEMALE_GREEN_BORDER = 25.8;
	private static final double BMI_FEMALE_ORANGE_BORDER = 32.2;

	private int getBMIColor(double BMI, double yelloBorder, double greenBorder,
			double orangeBorder) {
		if (BMI == 0) {
			return getResources().getColor(R.color.black);
		} else if (BMI < yelloBorder) {
			return getResources().getColor(R.color.yellow);
		} else if (BMI >= yelloBorder && BMI <= greenBorder) {
			return getResources().getColor(R.color.green);
		} else if (BMI > greenBorder && BMI <= orangeBorder) {
			return getResources().getColor(R.color.orange);
		}
		return getResources().getColor(R.color.red);
	}

	private void displayBMI(double BMI) {
		int color;
		if (gender.contentEquals("Male")) {
			color = getBMIColor(BMI, BMI_MALE_YELLOW_BORDER,
					BMI_MALE_GREEN_BORDER, BMI_MALE_ORANGE_BORDER);
		} else {
			color = getBMIColor(BMI, BMI_FEMALE_YELLOW_BORDER,
					BMI_FEMALE_GREEN_BORDER, BMI_FEMALE_ORANGE_BORDER);
		}
		textViewBMI.setTextColor(color);
		showBodyValueOnTextView(textViewBMI, BMI, "/", textViewBMIBonusText);
	}

	@Override
	protected void initGraphicComponents() {
		typeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KGBeStillAndKnow.ttf");
		textViewDate = (TextView) findViewById(R.id.textView_date_final);
		textViewWeight = (TextView) findViewById(R.id.textView_weight_final);
		textViewWaist = (TextView) findViewById(R.id.textView_waist_final);
		textViewBiceps = (TextView) findViewById(R.id.textView_biceps_final);
		textViewChest = (TextView) findViewById(R.id.textView_chest_final);
		textViewBMI = (TextView) findViewById(R.id.textView_bmi_final);
		imageViewFrontPicture = (ImageView) findViewById(R.id.imageView_front_picture_final);
		imageViewSidePicture = (ImageView) findViewById(R.id.imageView_side_picture_final);
		imageViewBackPicture = (ImageView) findViewById(R.id.imageView_back_picture_final);
		imageButtonPreviusPage = (ImageButton) findViewById(R.id.imageButton_previus_page);
		imageButtonNextPage = (ImageButton) findViewById(R.id.imageButton_next_page);

        buttonDeleteSaveBodyEntry = (Button) findViewById(R.id.button_delete_save_body_entry);
        buttonDeleteSaveBodyEntry.setTypeface(typeFace);
		// Log.i("Button", imageButtonSlideShow.toString());

		textViewDate.setTypeface(typeFace);
		textViewWeight.setTypeface(typeFace);
		textViewWaist.setTypeface(typeFace);
		textViewBiceps.setTypeface(typeFace);
		textViewChest.setTypeface(typeFace);
		textViewBMI.setTypeface(typeFace);
		service = new BodyService(this);

	}

	public void startPreviusPage(View v) {
		currentPosition = currentPosition - 1;
		displayEntry();
	}

	public void startNextPage(View v) {
		currentPosition = currentPosition + 1;
		displayEntry();
	}

	public void startDeletePage(View v) {
		service.deleteEntry(entries.get(currentPosition));
		getEntries();
	}

	private void hideAndDisplayButtons() {
		if (currentPosition == 0) {
			imageButtonPreviusPage.setVisibility(View.INVISIBLE);
		} else {
            imageButtonPreviusPage.setVisibility(View.VISIBLE);
		}

		if (currentPosition == endPosition) {
			imageButtonNextPage.setVisibility(View.INVISIBLE);
		} else {
			imageButtonNextPage.setVisibility(View.VISIBLE);
		}
	}

	public void showFrontPicture(View v) {
		Uri uri = entry.getFrontPictureUri();
		if (this.checkUriForValue(uri)) {
			DialogShowFullSizedPicture.createDialog(this, uri);
		}
	}

	public void showSidePicture(View v) {
		Uri uri = entry.getSidePictureUri();
		if (this.checkUriForValue(uri)) {
			DialogShowFullSizedPicture.createDialog(this, uri);
		}
	}

	public void showBackPicture(View v) {
		Uri uri = entry.getBackPictureUri();
		if (this.checkUriForValue(uri)) {
			DialogShowFullSizedPicture.createDialog(this, uri);
		}
	}

	private boolean checkUriForValue(Uri uri) {
		if (uri != null && uri.getPath().isEmpty()) {
			return false;
		}
		return true;
	}

	public void slideShow(View v) {
		PopupMenu popup = new PopupMenu(this, imageButtonSlideShow);
		popup.inflate(R.menu.see_body_parts_slide_show_options);
		popup.setOnMenuItemClickListener(this);
		popup.show();
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.show_front_pictures_slideshow:
			Log.i("SlideShow", "Front");
			List<Uri> frontUriList = service.getAllFrontUriEntries();
			DialogShowFullSizedPictureSlideShow
					.createDialog(this, frontUriList);
			return true;
		case R.id.show_side_pictures_slideshow:
			List<Uri> sideUriList = service.getAllSideUriEntries();
			DialogShowFullSizedPictureSlideShow.createDialog(this, sideUriList);
			Log.i("SlideShow", "Side");
			return true;
		case R.id.show_back_pictures_slideshow:
			List<Uri> backUriList = service.getAllBackUriEntries();
			DialogShowFullSizedPictureSlideShow.createDialog(this, backUriList);
			Log.i("SlideShow", "Front");
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
