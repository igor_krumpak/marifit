package si.oranza.marifit.activity;

import si.iitech.library.activity.IITechPictureActivity;
import si.oranza.marifit.service.FoodService;
import android.app.ActionBar;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import si.oranza.marifit.R;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class TakeFoodPictureActivity extends IITechPictureActivity {

	private Uri uri;

	private ImageView imageViewTakenFoodPicture;
	private EditText editTextFoodPictureComment;
	private EditText editTextFoodPictureCalories;
    private TextView textViewFoodPictureCaloriesTitle;


	private String alertDialogMessage;
	private String alertDialogYesMessage;
	private String alertDialogNoMessage;

	private FoodService service;
	private Typeface typeFace;

    private Button countCalories;
    private Button retakeFoodPhoto;
    private Button deleteFoodPhoto;
    private Button saveFoodPhoto;

	private DisplayImageOptions options;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_take_food_picture);
		initGraphicComponents();
		initImageLoaderDisplayOptions();
		uri = useCamera();
	}

	private void initImageLoaderDisplayOptions() {
		options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(
						R.drawable.see_food_picture_sample_picture)
				.displayer(new FadeInBitmapDisplayer(500)).cacheInMemory()
				.cacheOnDisc()
				.showImageOnFail(R.drawable.see_food_picture_sample_picture)
				.build();
	}

	@Override
	protected void initGraphicComponents() {
		typeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KGBeStillAndKnow.ttf");
		editTextFoodPictureComment = (EditText) findViewById(R.id.editText_food_picture_comment);
		editTextFoodPictureComment.setTypeface(typeFace);
		editTextFoodPictureCalories = (EditText) findViewById(R.id.editText_food_picture_calories);
		editTextFoodPictureCalories.setTypeface(typeFace);
		imageViewTakenFoodPicture = (ImageView) findViewById(R.id.imageView_taken_food_picture);
		alertDialogMessage = getResources().getString(
				R.string.alert_dialog_message_save);
		alertDialogYesMessage = getResources().getString(
				R.string.alert_dialog_yes_message);
		alertDialogNoMessage = getResources().getString(
				R.string.alert_dialog_no_message);

        countCalories = (Button) findViewById(R.id.button_count_calories);
        retakeFoodPhoto = (Button) findViewById(R.id.button_retake_food_photo);
        deleteFoodPhoto = (Button) findViewById(R.id.button_delete_food_photo);
        saveFoodPhoto = (Button) findViewById(R.id.button_save_food_photo);
        countCalories.setTypeface(typeFace);
        retakeFoodPhoto.setTypeface(typeFace);
        deleteFoodPhoto.setTypeface(typeFace);
        saveFoodPhoto.setTypeface(typeFace);

        textViewFoodPictureCaloriesTitle = (TextView) findViewById(R.id.textView_food_picture_calories_title);
        textViewFoodPictureCaloriesTitle.setTypeface(typeFace);

		service = new FoodService(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				ImageLoader.getInstance().displayImage(uri.toString(),
						imageViewTakenFoodPicture, options);
			} else {
				this.finish();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPostResume() {
		super.onPostResume();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	public void startRetakeAPhoto(View v) {
		uri = useCamera();
	}

	public void startCountCalories(View v) {

	}

	public void startCancelPhoto(View v) {
		this.finish();
	}

	public void startSavePhoto(View v) {
		if (checkIfEditTextIsEmpty(editTextFoodPictureCalories)) {
			createYesNoAlertDialog(alertDialogMessage, alertDialogYesMessage,
					alertDialogNoMessage, dialogClickListener);
		} else {
			saveFood();
		}

	}

	private void saveFood() {
		service.createNewFoodEntry(uri, editTextFoodPictureComment.getText()
				.toString(), editTextFoodPictureCalories.getText().toString());
		this.finish();
	}

	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				log("Positive button?");
				saveFood();
				break;
			}
		}
	};

}
