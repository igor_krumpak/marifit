package si.oranza.marifit.activity;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import si.iitech.library.util.Serialize;
import si.oranza.marifit.R;
import si.oranza.marifit.objects.SettingsObject;

/**
 * Created by igor on 25.1.2015.
 */
public class SettingsActivity extends Activity {

    private Typeface typeFace;
    private TextView textViewTitle1;
    private TextView textViewTitle2;
    private TextView textViewTitle3;
    private EditText editTextWeight;
    private ImageButton imageButtonSave;
    private ImageButton imageButtonFemale;
    private ImageButton imageButtonMale;
    private String sex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initGraphicComponents();
        getCurrentData();
    }

    private void getCurrentData() {
        SettingsObject temp = Serialize.deserialize(this, SettingsObject.class, "settings1", "set");
        if(temp == null) {
            sex = "Male";
            editTextWeight.setText("170");
        } else {
            sex = temp.getSex();
            editTextWeight.setText(temp.getHeight() + "");
        }
        displaySex();
    }

    private void displaySex() {
        if(sex.contentEquals("Male")) {
            imageButtonFemale.setImageResource(R.drawable.radio_button_female_click_selected);
            imageButtonMale.setImageResource(R.drawable.radio_button_male_click);
        } else {
            imageButtonMale.setImageResource(R.drawable.radio_button_male_click_selected);
            imageButtonFemale.setImageResource(R.drawable.radio_button_female_click);
        }
    }

    private void initGraphicComponents() {
        typeFace = Typeface.createFromAsset(getAssets(),
                "fonts/KGBeStillAndKnow.ttf");
        textViewTitle1 = (TextView)findViewById(R.id.textView_title_1);
        textViewTitle1.setTypeface(typeFace);
        textViewTitle2 = (TextView)findViewById(R.id.textView_title_2);
        textViewTitle2.setTypeface(typeFace);
        textViewTitle3 = (TextView)findViewById(R.id.textView_title_3);
        textViewTitle3.setTypeface(typeFace);
        editTextWeight = (EditText)findViewById(R.id.editText_weight);
        editTextWeight.setTypeface(typeFace);
        imageButtonFemale = (ImageButton)findViewById(R.id.imageButton_female);
        imageButtonMale = (ImageButton)findViewById(R.id.imageButton_male);
        imageButtonSave = (ImageButton)findViewById(R.id.imageButton_save);
    }

    public void setMale(View v) {
        sex = "Male";
        displaySex();
    }

    public void setFemale(View v) {
        sex = "Female";
        displaySex();
    }

    public void save(View v) {
        String weightText = editTextWeight.getText().toString();
        if(!weightText.isEmpty()) {
            SettingsObject settingsObject = new SettingsObject();
            settingsObject.setSex(sex);
            settingsObject.setHeight(Integer.parseInt(weightText));
            Serialize.serialize(this,settingsObject,"settings1", "set");
            this.finish();
        } else {
            Toast.makeText(this, getResources().getString(R.string.no_weight_entry), Toast.LENGTH_LONG).show();
        }

    }
}
