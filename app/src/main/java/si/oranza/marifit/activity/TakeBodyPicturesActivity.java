package si.oranza.marifit.activity;

import si.iitech.library.activity.IITechPictureActivity;
import si.oranza.marifit.dao.BodyDAO;
import si.oranza.marifit.dialog.DialogShowFullSizedPicture;
import si.oranza.marifit.service.BodyService;
import si.oranza.marifit.util.OranzaUtility;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import si.oranza.marifit.R;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class TakeBodyPicturesActivity extends IITechPictureActivity implements
		OnClickListener, OnMenuItemClickListener {

	private TextView textViewDate;
	private EditText editTextWeight;
	private EditText editTextWaist;
	private EditText editTextChest;
	private EditText editTextBiceps;

    private TextView textViewWeight;
    private TextView textViewWaist;
    private TextView textViewChest;
    private TextView textViewBiceps;

	private ImageView imageViewFrontPicture;
	private ImageView imageViewSidePicture;
	private ImageView imageViewBackPicture;

	private final int TAKE_FRONT_PICTURE_REQUEST = 0x2;
	private final int TAKE_SIDE_PICTURE_REQUEST = 0x3;
	private final int TAKE_BACK_PICTURE_REQUEST = 0x4;

	private Uri frontPictureUri;
	private Uri sidePictureUri;
	private Uri backPictureUri;

	private DisplayImageOptions optionsFrontPicture;
	private DisplayImageOptions optionsSidePicture;
	private DisplayImageOptions optionsBackPicture;

	private long time;

	private BodyService service;

	private BodyDAO entry;

	private Typeface typeFace;

    private Button deleteBodyEntry;
    private Button saveBodyEntry;
    private Button frontPicture;
    private Button backPicture;
    private Button sidePicture;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_take_body_pictures);
		getCurrentTime();
		initGraphicComponents();
		initImageLoaderDisplayOptionsFront();
		initImageLoaderDisplayOptionsSide();
		initImageLoaderDisplayOptionsBack();
		getLatestEntry();
	}

	private void initImageLoaderDisplayOptionsFront() {
		optionsFrontPicture = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(
						R.drawable.take_body_front_picture_background)
				.displayer(new FadeInBitmapDisplayer(500))
				.showImageOnFail(R.drawable.take_body_front_picture_background)
				.build();
	}

	private void initImageLoaderDisplayOptionsSide() {
		optionsSidePicture = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(
						R.drawable.take_body_side_picture_background)
				.displayer(new FadeInBitmapDisplayer(500))
				.showImageOnFail(R.drawable.take_body_side_picture_background)
				.build();
	}

	private void initImageLoaderDisplayOptionsBack() {
		optionsBackPicture = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(
						R.drawable.take_body_back_picture_background)
				.displayer(new FadeInBitmapDisplayer(500))
				.showImageOnFail(R.drawable.take_body_back_picture_background)
				.build();
	}

	private void getCurrentTime() {
		time = OranzaUtility.getCurrentTime();

	}

	private void showBodyValueOnEditText(EditText editText, int value,
			String bonusText) {
		if (0 != value) {
			editText.setText(bonusText + value);
		}
	}

	private void getLatestEntry() {
		entry = service.getLatestEntry(time);

        if (null != entry) {
			showBodyValueOnEditText(editTextWeight, entry.getWeight(),
					"");
			showBodyValueOnEditText(editTextWaist, entry.getWaist(),
					"");
			showBodyValueOnEditText(editTextChest, entry.getChest(),
					"");
			showBodyValueOnEditText(editTextBiceps, entry.getBiceps(),
					"");

			frontPictureUri = entry.getFrontPictureUri();
			sidePictureUri = entry.getSidePictureUri();
			backPictureUri = entry.getBackPictureUri();
		}

		displayPictureOnImageView(frontPictureUri, imageViewFrontPicture,
				optionsFrontPicture);
		displayPictureOnImageView(sidePictureUri, imageViewSidePicture,
				optionsSidePicture);
		displayPictureOnImageView(backPictureUri, imageViewBackPicture,
				optionsBackPicture);
	}

	private void displayPictureOnImageView(Uri uri, ImageView imageView,
			DisplayImageOptions options) {
		String uriString = uri.toString();
		ImageLoader.getInstance().displayImage(uriString, imageView, options);
	}

	@Override
	protected void initGraphicComponents() {
		typeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KGBeStillAndKnow.ttf");
		textViewDate = (TextView) findViewById(R.id.textView_current_date);
		textViewDate.setText(OranzaUtility.getCurrentDateInFormatedString());
		textViewDate.setTypeface(typeFace);

        textViewWeight = (TextView) findViewById(R.id.textView_weight);
        textViewWaist = (TextView) findViewById(R.id.textView_waist);
        textViewChest = (TextView) findViewById(R.id.textView_chest);
        textViewBiceps = (TextView) findViewById(R.id.textView_biceps);

		editTextWeight = (EditText) findViewById(R.id.editText_weight);
		editTextWaist = (EditText) findViewById(R.id.editText_waist);
		editTextChest = (EditText) findViewById(R.id.editText_chest);
		editTextBiceps = (EditText) findViewById(R.id.editText_biceps);

		editTextWaist = (EditText) findViewById(R.id.editText_waist);
		editTextChest = (EditText) findViewById(R.id.editText_chest);
		editTextBiceps = (EditText) findViewById(R.id.editText_biceps);

        deleteBodyEntry = (Button) findViewById(R.id.button_delete_body_entry);
        saveBodyEntry = (Button) findViewById(R.id.button_save_body_entry);

        frontPicture = (Button) findViewById(R.id.button_front_picture);
        backPicture = (Button) findViewById(R.id.button_back_picture);
        sidePicture = (Button) findViewById(R.id.button_side_picture);

		editTextWeight.setTypeface(typeFace);
		editTextWaist.setTypeface(typeFace);
		editTextChest.setTypeface(typeFace);
		editTextBiceps.setTypeface(typeFace);
        textViewWeight.setTypeface(typeFace);
        textViewWaist.setTypeface(typeFace);
        textViewChest.setTypeface(typeFace);
        textViewBiceps.setTypeface(typeFace);
        deleteBodyEntry.setTypeface(typeFace);
        saveBodyEntry.setTypeface(typeFace);
        frontPicture.setTypeface(typeFace);
        backPicture.setTypeface(typeFace);
        sidePicture.setTypeface(typeFace);

		imageViewFrontPicture = (ImageView) findViewById(R.id.imageView_front_picture);
		imageViewSidePicture = (ImageView) findViewById(R.id.imageView_side_picture);
		imageViewBackPicture = (ImageView) findViewById(R.id.imageView_back_picture);


		frontPictureUri = Uri.parse("");
		sidePictureUri = Uri.parse("");
		backPictureUri = Uri.parse("");

		imageViewFrontPicture.setOnClickListener(this);
		imageViewSidePicture.setOnClickListener(this);
		imageViewBackPicture.setOnClickListener(this);
		service = new BodyService(this);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case TAKE_FRONT_PICTURE_REQUEST:
			log("FRONT");
			displayPictureOnImageView(frontPictureUri, imageViewFrontPicture,
					optionsFrontPicture);
			break;
		case TAKE_SIDE_PICTURE_REQUEST:
			log("SIDE");
			displayPictureOnImageView(sidePictureUri, imageViewSidePicture,
					optionsSidePicture);
			break;
		case TAKE_BACK_PICTURE_REQUEST:
			log("BACK");
			displayPictureOnImageView(backPictureUri, imageViewBackPicture,
					optionsBackPicture);
			break;
		}
	}

	public void startTakeFrontPicture(View v) {
		frontPictureUri = this.useCamera(TAKE_FRONT_PICTURE_REQUEST);
	}

	public void startTakeSidePicture(View v) {
		sidePictureUri = this.useCamera(TAKE_SIDE_PICTURE_REQUEST);
	}

	public void startTakeBackPicture(View v) {
		backPictureUri = this.useCamera(TAKE_BACK_PICTURE_REQUEST);
	}

	public void startCancelEntry(View v) {
		this.finish();
	}

	private boolean checkUriForValue(Uri uri) {
		if (uri != null && uri.getPath().isEmpty()) {
			return true;
		}
		return false;
	}

	public void startSaveEntry(View v) {
		if (null == entry) {
			entry = new BodyDAO(time, 0, this.getValueFromEditText(
					editTextWeight, 0, ""),
					this.getValueFromEditText(editTextWaist, 0,
							""), this.getValueFromEditText(
							editTextBiceps, 0, ""),
					this.getValueFromEditText(editTextChest, 0,
							""), frontPictureUri,
					sidePictureUri, backPictureUri);
			service.insertEntry(entry);
		} else {
			entry.setWeight(this.getValueFromEditText(editTextWeight, 0,
					""));
			entry.setWaist(this.getValueFromEditText(editTextWaist, 0,
					""));
			entry.setBiceps(this.getValueFromEditText(editTextBiceps, 0,
                    ""));
			entry.setChest(this.getValueFromEditText(editTextChest, 0,
                    ""));
			entry.setFrontPictureUri(checkUriForValueAndAssignOne(frontPictureUri));
			entry.setSidePictureUri(checkUriForValueAndAssignOne(sidePictureUri));
			entry.setBackPictureUri(checkUriForValueAndAssignOne(backPictureUri));
			service.updateEntry(entry);
		}
		log(entry.toString());
		this.finish();
	}

	private Uri checkUriForValueAndAssignOne(Uri pictureUri) {
		// if(pictureUri == null && pictureUri.getPath().isEmpty()) {
		// return new Uri.Builder().build();
		// }
		return pictureUri;
	}

	private int getValueFromEditText(EditText editText, int value,
			String bunusText) {
		String temp = editText.getText().toString();
		if (!temp.isEmpty()) {
			return Integer.parseInt(temp.replace(bunusText, "").trim());
		}
		return value;
	}

	@Override
	public void onClick(View v) {
        /**
		switch (v.getId()) {
		case R.id.imageView_front_picture:
			if (this.checkUriForValue(frontPictureUri)) {
				PopupMenu popup = new PopupMenu(this, imageViewFrontPicture);
				popup.inflate(R.menu.take_body_pictures_front_picture_options);
				popup.setOnMenuItemClickListener(this);
				popup.show();
			}
			break;
		case R.id.imageView_side_picture:
			if (this.checkUriForValue(sidePictureUri)) {
				PopupMenu popup = new PopupMenu(this, imageViewFrontPicture);
				popup.inflate(R.menu.take_body_pictures_side_picture_options);
				popup.setOnMenuItemClickListener(this);
				popup.show();
			}
			break;
		case R.id.imageView_back_picture:
			if (this.checkUriForValue(backPictureUri)) {
				PopupMenu popup = new PopupMenu(this, imageViewFrontPicture);
				popup.inflate(R.menu.take_body_pictures_back_picture_options);
				popup.setOnMenuItemClickListener(this);
				popup.show();
			}
			break;
		}
         */
	}

	// RETAKE

	private void showValueOnTextView(EditText editText, String value,
			String defaultValue, String bonusText) {
		if (value.isEmpty()) {
			editText.setText(defaultValue);
		} else {
			editText.setText(value + bonusText);
		}

	}

	DialogInterface.OnClickListener frontPictureRetakeListner = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				frontPictureUri = useCamera(TAKE_FRONT_PICTURE_REQUEST);
				break;
			}
		}
	};

	DialogInterface.OnClickListener sidePictureRetakeListner = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				sidePictureUri = useCamera(TAKE_FRONT_PICTURE_REQUEST);
				break;
			}
		}
	};

	DialogInterface.OnClickListener backPictureRetakeListner = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				backPictureUri = useCamera(TAKE_FRONT_PICTURE_REQUEST);
				break;
			}
		}
	};

	private InputFilter[] returnShortFilter() {
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(3);
		return FilterArray;
	}

	private InputFilter[] returnLongFilter() {
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(20);
		return FilterArray;
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.show_front_image:
			DialogShowFullSizedPicture.createDialog(this, frontPictureUri);
			return true;
		case R.id.delete_front_image:
			frontPictureUri = Uri.parse("");
			displayPictureOnImageView(frontPictureUri, imageViewFrontPicture,
					optionsFrontPicture);
			return true;
		case R.id.show_side_image:
			DialogShowFullSizedPicture.createDialog(this, sidePictureUri);
			return true;
		case R.id.delete_side_image:
			sidePictureUri = Uri.parse("");
			displayPictureOnImageView(sidePictureUri, imageViewSidePicture,
					optionsSidePicture);
			return true;
		case R.id.show_back_image:
			DialogShowFullSizedPicture.createDialog(this, backPictureUri);
			return true;
		case R.id.delete_back_image:
			backPictureUri = Uri.parse("");
			displayPictureOnImageView(backPictureUri, imageViewBackPicture,
					optionsBackPicture);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
