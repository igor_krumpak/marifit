package si.oranza.marifit.database;

import java.util.ArrayList;

import si.oranza.marifit.dao.BodyDAO;
import si.oranza.marifit.dao.FoodDAO;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

public class OranzaDB extends SQLiteOpenHelper {

	private static final int DB_VERSION = 24;
	private static final String DB_NAME = "FitnesHelperDB";

	public OranzaDB(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	private static final String OBJECT_ID = "id";
	private static final String OBJECT_TIME = "time";
	private static final String OBJECT_URI = "uri";

	private static final String DB_TABLE_FOOD = "food";
	private static final String FOOD_COMMENT = "comment";
	private static final String FOOD_CALORIES = "calories";
	private static final String CREATE_DB_FOOD = "CREATE TABLE "
			+ DB_TABLE_FOOD + "(" + OBJECT_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + OBJECT_TIME + " INTEGER,"
			+ OBJECT_URI + " TEXT," + FOOD_COMMENT + " TEXT," + FOOD_CALORIES
			+ " INTEGER" + ")";

	private static final String DB_TABLE_BODY = "body";
	private static final String BODY_WEIGHT = "weight";
	private static final String BODY_WAIST = "waist_volume";
	private static final String BODY_CHEST = "chest_volume";
	private static final String BODY_BICEPS = "biceps_volume";
	private static final String BODY_FRONT_PICTURE_URI = "front_picture_uri";
	private static final String BODY_SIDE_PICTURE_URI = "side_picture_uri";
	private static final String BODY_BACK_PICTURE_URI = "back_picture_uri";
	private static final String CREATE_DB_BODY = "CREATE TABLE "
			+ DB_TABLE_BODY + "(" + OBJECT_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + OBJECT_TIME
			+ " INTEGER," + BODY_WEIGHT + " INTEGER, " + BODY_WAIST + " INTEGER, "
			+ BODY_CHEST + " INTEGER, " + BODY_BICEPS + " INTEGER, "
			+ BODY_FRONT_PICTURE_URI + " TEXT, " + BODY_SIDE_PICTURE_URI
			+ " TEXT, " + BODY_BACK_PICTURE_URI + " TEXT" + ")";

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_DB_FOOD);
		db.execSQL(CREATE_DB_BODY);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(SQLiteOpenHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_FOOD);
		db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_BODY);
		onCreate(db);
	}

	// public void updateBodyObject(BodyObject bodyObject) {
	// SQLiteDatabase db = this.getWritableDatabase();
	// ContentValues values = new ContentValues();
	// // values.put(BODY_ID, bodyObject.getId());
	// values.put(BODY_TIME, bodyObject.getTime());
	// values.put(BODY_WEIGHT, bodyObject.getWeight());
	// values.put(BODY_WAIST_VOLUME, bodyObject.getWaistVolume());
	// values.put(BODY_CHEST_VOLUME, bodyObject.getChestVolume());
	// values.put(BODY_BICEPS_VOLUME, bodyObject.getBicepsVolume());
	// values.put(BODY_FRONT_PICTURE_PATH, bodyObject.getFrontPicturePath());
	// values.put(BODY_SIDE_PICTURE_PATH, bodyObject.getSidePicturePath());
	// values.put(BODY_BACK_PICTURE_PATH, bodyObject.getBackPicturePath());
	// db.update(DB_TABLE_BODY, values, BODY_ID + "=" + bodyObject.getId(),
	// null);
	// db.close();
	// }

	// Delete statements

	public int deleteBodyObject(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		return db.delete(DB_TABLE_BODY, OBJECT_ID + "=" + id, null);
	}

	// FoodDAO

	public void createFoodEntry(FoodDAO object) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(OBJECT_TIME, object.getTime());
		values.put(OBJECT_URI, object.getUri().toString());
		values.put(FOOD_COMMENT, object.getComment());
		values.put(FOOD_CALORIES, object.getCalories());
		db.insert(DB_TABLE_FOOD, null, values);
		db.close();
	}

	public ArrayList<FoodDAO> returnAllFoodEntries() {
		ArrayList<FoodDAO> objectArrayList = new ArrayList<FoodDAO>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(DB_TABLE_FOOD, new String[] { OBJECT_ID,
				OBJECT_TIME, OBJECT_URI, FOOD_COMMENT, FOOD_CALORIES }, null,
				null, null, null, OBJECT_TIME + " DESC");
		if (cursor.moveToFirst()) {
			do {
				FoodDAO object = new FoodDAO(cursor.getLong(1),
						Uri.parse(cursor.getString(2)), cursor.getInt(0),
						cursor.getString(3), cursor.getInt(4));
				objectArrayList.add(object);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return objectArrayList;
	}

	public int deleteFoodEntry(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		return db.delete(DB_TABLE_FOOD, OBJECT_ID + "=" + id, null);
	}

	public FoodDAO returnFoodEntry(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(DB_TABLE_FOOD, new String[] { OBJECT_ID,
				OBJECT_TIME, OBJECT_URI, FOOD_COMMENT, FOOD_CALORIES },
				OBJECT_ID + "=" + id, null, null, null, null);
		FoodDAO object = null;
		if (cursor.moveToFirst()) {
			object = new FoodDAO(cursor.getLong(1), Uri.parse(cursor
					.getString(2)), cursor.getInt(0), cursor.getString(3),
					cursor.getInt(4));
		}
		cursor.close();
		return object;
	}

	// BodyDAO

	public int deleteBodyEntry(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		return db.delete(DB_TABLE_BODY, OBJECT_ID + "=" + id, null);
	}

	public void createBodyEntry(BodyDAO object) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(OBJECT_TIME, object.getTime());
		values.put(BODY_WEIGHT, object.getWeight());
		values.put(BODY_WAIST, object.getWaist());
		values.put(BODY_CHEST, object.getChest());
		values.put(BODY_BICEPS, object.getBiceps());
		values.put(BODY_FRONT_PICTURE_URI, object.getFrontPictureUri()
				.toString());
		values.put(BODY_SIDE_PICTURE_URI, object.getSidePictureUri().toString());
		values.put(BODY_BACK_PICTURE_URI, object.getBackPictureUri().toString());
		db.insert(DB_TABLE_BODY, null, values);
		db.close();
	}

	public BodyDAO returnBodyEntry(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(DB_TABLE_BODY, new String[] { OBJECT_ID,
				OBJECT_TIME, BODY_WEIGHT, BODY_WAIST, BODY_CHEST, BODY_BICEPS,
				BODY_FRONT_PICTURE_URI, BODY_SIDE_PICTURE_URI,
				BODY_BACK_PICTURE_URI }, OBJECT_ID + "=" + id, null, null,
				null, null);
		BodyDAO object = null;
		if (cursor.moveToFirst()) {
			object = new BodyDAO(cursor.getLong(1), cursor.getInt(0),
					cursor.getInt(2), cursor.getInt(3),
					cursor.getInt(4), cursor.getInt(5), Uri.parse(cursor
							.getString(6)), Uri.parse(cursor.getString(7)),
					Uri.parse(cursor.getString(8)));

		}
		cursor.close();
		return object;
	}

	public ArrayList<BodyDAO> returnAllBodyEntries() {
		ArrayList<BodyDAO> objectArrayList = new ArrayList<BodyDAO>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(DB_TABLE_BODY, new String[] { OBJECT_ID,
				OBJECT_TIME, BODY_WEIGHT, BODY_WAIST, BODY_CHEST, BODY_BICEPS,
				BODY_FRONT_PICTURE_URI, BODY_SIDE_PICTURE_URI,
				BODY_BACK_PICTURE_URI }, null, null, null, null, OBJECT_TIME
				+ " DESC");
		if (cursor.moveToFirst()) {
			do {
				BodyDAO object = new BodyDAO(cursor.getLong(1),
						cursor.getInt(0), cursor.getInt(2),
						cursor.getInt(3), cursor.getInt(4),
						cursor.getInt(5), Uri.parse(cursor.getString(6)),
						Uri.parse(cursor.getString(7)), Uri.parse(cursor
								.getString(8)));
				if (!object.checkIfEmpty()) {
					objectArrayList.add(object);
				}
			} while (cursor.moveToNext());
		}
		cursor.close();
		return objectArrayList;
	}

	public BodyDAO returnLatestBodyEntry() {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(DB_TABLE_BODY, new String[] { OBJECT_ID,
				OBJECT_TIME, BODY_WEIGHT, BODY_WAIST, BODY_BICEPS, BODY_CHEST,
				BODY_FRONT_PICTURE_URI, BODY_SIDE_PICTURE_URI,
				BODY_BACK_PICTURE_URI }, null, null, null, null, OBJECT_TIME
				+ " DESC");
		BodyDAO object = null;
		if (cursor.moveToFirst()) {
			object = new BodyDAO(cursor.getLong(1), cursor.getInt(0),
					cursor.getInt(2), cursor.getInt(3),
					cursor.getInt(4), cursor.getInt(5), Uri.parse(cursor
							.getString(6)), Uri.parse(cursor.getString(7)),
					Uri.parse(cursor.getString(8)));
			Log.i("returnLatestBodyEntry", object.toString());
		}
		cursor.close();
		return object;
	}

	public void updateEntry(BodyDAO object) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(OBJECT_TIME, object.getTime());
		values.put(BODY_WEIGHT, object.getWeight());
		values.put(BODY_WAIST, object.getWaist());
		values.put(BODY_CHEST, object.getChest());
		values.put(BODY_BICEPS, object.getBiceps());
		values.put(BODY_FRONT_PICTURE_URI, object.getFrontPictureUri()
				.toString());
		values.put(BODY_SIDE_PICTURE_URI, object.getSidePictureUri().toString());
		values.put(BODY_BACK_PICTURE_URI, object.getBackPictureUri().toString());
		db.update(DB_TABLE_BODY, values, OBJECT_ID + "=" + object.getId(), null);
		db.close();
	}
}
