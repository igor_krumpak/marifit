package si.oranza.marifit.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import si.oranza.marifit.dao.BodyDAO;
import si.oranza.marifit.database.OranzaDB;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

public class BodyService {

    private OranzaDB db;

    public BodyService(final Context context) {
        db = new OranzaDB(context);
    }

    public void insertEntry(BodyDAO entry) {
        db.createBodyEntry(entry);
        Log.i("Entries", db.returnAllBodyEntries().size() + "");
    }

    public BodyDAO getLatestEntry(long time) {
        BodyDAO entry = db.returnLatestBodyEntry();
        if (null != entry) {
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTimeInMillis(time);
            cal2.setTimeInMillis(entry.getTime());
            boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
            if (sameDay) {
                return entry;
            }
        }
        return null;
    }

    public void updateEntry(BodyDAO entry) {
        db.updateEntry(entry);
        Log.i("Entries", db.returnAllBodyEntries().size() + "");

    }

    public List<BodyDAO> getAllEntries() {
        List<BodyDAO> entries = db.returnAllBodyEntries();
        return entries;
    }

    public List<Uri> getAllFrontUriEntries() {
        List<BodyDAO> entries = db.returnAllBodyEntries();
        List<Uri> frontUriList = new ArrayList<Uri>();
        for (BodyDAO dao : entries) {
            Uri frontUri = dao.getFrontPictureUri();
            if (!frontUri.toString().isEmpty()) {
                frontUriList.add(frontUri);
            }
        }
        if (entries.size() == 0) {
            return null;
        }
        return frontUriList;
    }

    public boolean deleteEntry(BodyDAO entry) {
        db.deleteBodyEntry(entry.getId());
        return true;
    }

    public List<Uri> getAllSideUriEntries() {
        List<BodyDAO> entries = db.returnAllBodyEntries();
        List<Uri> sideUriList = new ArrayList<Uri>();
        for (BodyDAO dao : entries) {
            Uri sideUri = dao.getSidePictureUri();
            if (!sideUri.toString().isEmpty()) {
                sideUriList.add(sideUri);
            }
        }
        if (entries.size() == 0) {
            return null;
        }
        return sideUriList;
    }

    public List<Uri> getAllBackUriEntries() {
        List<BodyDAO> entries = db.returnAllBodyEntries();
        List<Uri> backUriList = new ArrayList<Uri>();
        for (BodyDAO dao : entries) {
            Uri backUri = dao.getBackPictureUri();
            if (!backUri.toString().isEmpty()) {
                backUriList.add(backUri);
            }
        }
        if (entries.size() == 0) {
            return null;
        }
        return backUriList;
    }

}
