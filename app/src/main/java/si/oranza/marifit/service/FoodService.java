package si.oranza.marifit.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import si.oranza.marifit.dao.FoodDAO;
import si.oranza.marifit.database.OranzaDB;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

public class FoodService {

	private OranzaDB db;

	public FoodService(final Context context) {
		db = new OranzaDB(context);
	}

	public void createNewFoodEntry(final Uri uri, final String comment,
			final String calories) {
		Log.i("FoodService", "createNewFoodEntry");
		FoodDAO object;
		if (0 == calories.length()) {
			object = new FoodDAO(System.currentTimeMillis(), uri, 0, comment,
					0);
		} else {
			object = new FoodDAO(System.currentTimeMillis(), uri, 0, comment,
					Integer.parseInt(calories));
		}
		db.createFoodEntry(object);
	}

	public ArrayList<FoodDAO> returnAllFoodEntries() {
		return db.returnAllFoodEntries();
	}

	public ArrayList<FoodDAO> returnAllFoodEntriesThisDay() {
		ArrayList<FoodDAO> returnList = new ArrayList<FoodDAO>();
		ArrayList<FoodDAO> list = db.returnAllFoodEntries();
		for (FoodDAO dao : list) {
			Calendar now = GregorianCalendar.getInstance();
			Calendar toCompare = GregorianCalendar.getInstance();
			toCompare.setTimeInMillis(dao.getTime());
			if (now.get(Calendar.DAY_OF_MONTH) == toCompare
					.get(Calendar.DAY_OF_MONTH)
					&& now.get(Calendar.MONTH) == toCompare.get(Calendar.MONTH)
					&& now.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
				returnList.add(dao);
			}
		}
		return returnList;
	}
	
	public ArrayList<FoodDAO> returnAllFoodEntriesThisWeek() {
		ArrayList<FoodDAO> returnList = new ArrayList<FoodDAO>();
		ArrayList<FoodDAO> list = db.returnAllFoodEntries();
		for (FoodDAO dao : list) {
			Calendar now = GregorianCalendar.getInstance();
			Calendar toCompare = GregorianCalendar.getInstance();
			toCompare.setTimeInMillis(dao.getTime());
			if (now.get(Calendar.WEEK_OF_YEAR) == toCompare
					.get(Calendar.WEEK_OF_YEAR)
					&& now.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
				returnList.add(dao);
			}
		}
		return returnList;
	}
	
	public ArrayList<FoodDAO> returnAllFoodEntriesThisMonth() {
		ArrayList<FoodDAO> returnList = new ArrayList<FoodDAO>();
		ArrayList<FoodDAO> list = db.returnAllFoodEntries();
		for (FoodDAO dao : list) {
			Calendar now = GregorianCalendar.getInstance();
			Calendar toCompare = GregorianCalendar.getInstance();
			toCompare.setTimeInMillis(dao.getTime());
			if (now.get(Calendar.MONTH) == toCompare.get(Calendar.MONTH)
					&& now.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
				returnList.add(dao);
			}
		}
		return returnList;
	}
	
	

	public boolean deleteFoodEntry(int id) {
		//FoodDAO foodEntry = returnFoodEntry((int) id);
		int rows = db.deleteFoodEntry(id);
		//IITechUtil.deleteImage(context, foodEntry.getUri());
		if (rows > 0) {
			return true;
		}
		return false;
	}

	public FoodDAO returnFoodEntry(int id) {
		return db.returnFoodEntry(id);
	}

	public void insertEntry(FoodDAO entry) {

	}

	public void updateEntry(FoodDAO entry) {
		// TODO Auto-generated method stub

	}

	public List<FoodDAO> getAllEntries() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean deleteEntry(FoodDAO entry) {
		// TODO Auto-generated method stub
		return false;
	}

}
