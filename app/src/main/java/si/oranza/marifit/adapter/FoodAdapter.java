package si.oranza.marifit.adapter;

import java.util.ArrayList;

import si.iitech.library.util.IITechUtil;
import si.oranza.marifit.R;
import si.oranza.marifit.dao.FoodDAO;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class FoodAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<FoodDAO> list;
	public FoodAdapter(final Context context, final ArrayList<FoodDAO> list) {
		this.context = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return list.get(position).getId();
	}

	private class FoodDAOHolder {
		TextView comment;
		TextView calories;
		TextView datetime;
		ImageView picture;
	}
	
	@Override
	public void notifyDataSetInvalidated() {
		Log.i("ADAPTER", "!!!");
		super.notifyDataSetInvalidated();
		
	}
	
	@Override
	public void notifyDataSetChanged() {
		Log.i("ADAPTER", "!!!");
		super.notifyDataSetChanged();
	}
	
	
	

	@Override
	public View getView(int position, View convertView, ViewGroup viewGroup) {
		FoodDAOHolder holder = null;
		LayoutInflater inflater = (LayoutInflater) this.context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.adapter_food, null);
			holder = new FoodDAOHolder();
			holder.comment = (TextView) convertView
					.findViewById(R.id.textView_comment);
			holder.calories = (TextView) convertView
					.findViewById(R.id.textView_calories);
			holder.picture = (ImageView) convertView
					.findViewById(R.id.imageView_food_picture);
			holder.datetime = (TextView) convertView
					.findViewById(R.id.textView_datetime);
			Typeface typeFace = Typeface.createFromAsset(context.getAssets(),
					"fonts/KGBeStillAndKnow.ttf");
			holder.comment.setTypeface(typeFace);
			holder.calories.setTypeface(typeFace);
			holder.datetime.setTypeface(typeFace);
			convertView.setTag(holder);
		} else {
			holder = (FoodDAOHolder) convertView.getTag();
		}
		ImageLoader.getInstance()
				.displayImage(list.get(position).getUri().toString(),
						holder.picture);
		String comment = list.get(position).getComment();
		if(0 == comment.length()) {
			holder.comment.setText("/");
		} else {
			holder.comment.setText(comment);
		}
		
		int calories = list.get(position).getCalories();
		if (calories > 0) {
			holder.calories.setText("Calories: " + calories + "");
		} else {
			holder.calories.setText("Calories: /");
		}
		holder.datetime.setText(IITechUtil.formatTimeToDMYhms(list.get(position)
				.getTime()));
		return convertView;
	}
	
	public void setList(ArrayList<FoodDAO> list) {
		this.list = list;
	}
	

}
