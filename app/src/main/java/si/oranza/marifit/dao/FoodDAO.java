package si.oranza.marifit.dao;

import android.net.Uri;

public class FoodDAO extends ObjectDAO {

	private String comment;
	private int calories;
	private Uri uri;

	public FoodDAO(long time, Uri uri, int id, String comment, int calories) {
		super(time, id);
		this.comment = comment;
		this.calories = calories;
		this.uri = uri;
	}

	public String getComment() {
		return comment;
	}

	public int getCalories() {
		return calories;
	}
	
	public Uri getUri() {
		return uri;
	}

}
