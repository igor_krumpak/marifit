package si.oranza.marifit.dao;


public class ObjectDAO {

	private long time;
	private int id;

	public ObjectDAO(long time, int id) {
		super();
		this.time = time;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public long getTime() {
		return time;
	}

}
