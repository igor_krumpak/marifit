package si.oranza.marifit.dao;

import android.net.Uri;

public class BodyDAO extends ObjectDAO {

	private int waist;
	private int weight;
	private int biceps;
	private int chest;

	private Uri frontPictureUri;
	private Uri sidePictureUri;
	private Uri backPictureUri;

	public BodyDAO(long time, int id, int weight, int waist,
			int biceps, int chest, Uri frontPictureUri,
			Uri sidePictureUri, Uri backPictureUri) {
		super(time, id);
		this.waist = waist;
		this.weight = weight;
		this.biceps = biceps;
		this.chest = chest;
		this.frontPictureUri = frontPictureUri;
		this.sidePictureUri = sidePictureUri;
		this.backPictureUri = backPictureUri;
	}

	public int getWaist() {
		return waist;
	}

	public void setWaist(int waist) {
		this.waist = waist;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getBiceps() {
		return biceps;
	}

	public void setBiceps(int biceps) {
		this.biceps = biceps;
	}

	public int getChest() {
		return chest;
	}

	public void setChest(int chest) {
		this.chest = chest;
	}

	public Uri getFrontPictureUri() {
		return frontPictureUri;
	}

	public void setFrontPictureUri(Uri frontPictureUri) {
		this.frontPictureUri = frontPictureUri;
	}

	public Uri getSidePictureUri() {
		return sidePictureUri;
	}

	public void setSidePictureUri(Uri sidePictureUri) {
		this.sidePictureUri = sidePictureUri;
	}

	public Uri getBackPictureUri() {
		return backPictureUri;
	}

	public void setBackPictureUri(Uri backPictureUri) {
		this.backPictureUri = backPictureUri;
	}

	public boolean checkIfEmpty() {
		if (0 == waist && 0 == weight && 0 == biceps && 0 == chest
				&& 0 == frontPictureUri.toString().length()
				&& 0 == sidePictureUri.toString().length()
				&& 0 == backPictureUri.toString().length()) {
			return true;
		}
		return false;

	}

	@Override
	public String toString() {
		return "BodyDAO [waist=" + waist + ", weight=" + weight + ", biceps="
				+ biceps + ", chest=" + chest + ", frontPictureUri="
				+ frontPictureUri + ", sidePictureUri=" + sidePictureUri
				+ ", backPictureUri=" + backPictureUri + ", getId()=" + getId()
				+ ", getTime()=" + getTime() + "]";
	}

}
