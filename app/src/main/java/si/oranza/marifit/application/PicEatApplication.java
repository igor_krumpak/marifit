package si.oranza.marifit.application;

import si.oranza.marifit.R;
import android.app.Application;
import android.util.Log;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class PicEatApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(
						R.drawable.see_food_picture_sample_picture)
				.displayer(new FadeInBitmapDisplayer(500))
				.showImageOnFail(R.drawable.see_food_picture_sample_picture)
				.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this).threadPoolSize(3).defaultDisplayImageOptions(options)
				.build();

		ImageLoader.getInstance().init(config);

		Log.i("Application", "onCreate");
	}
}
