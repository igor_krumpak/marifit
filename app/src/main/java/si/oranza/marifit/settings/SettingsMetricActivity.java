package si.oranza.marifit.settings;

import si.oranza.marifit.R;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;

public class SettingsMetricActivity extends PreferenceActivity {

	// private ListPreference unitsList;
	private ListPreference genderList;
	private ListPreference heightList;
	private CheckBoxPreference checkBoxPreferenceDaily;
	private ListPreference listPreferenceDailyNotificationTimeValues;

	// private String selectedUnitType;

	// private final String IMPERIAL = "Imperial";
	// private final String METRIC = "Metric";

	// private final String METRIC_DEFAULT_VALUE = "170";
	// private final String METRIC_DEFAULT_VALUE_DISPLAY = "170 cm";
	// private final String IMPERIAL_DEFAULT_VALUE = "67";
	// private final String IMPERIAL_DEFAULT_VALUE_DISPLAY = "5\'7\'\'";

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preference_metric);

		genderList = (ListPreference) findPreference("gender");
		heightList = (ListPreference) findPreference("height");
		checkBoxPreferenceDaily = (CheckBoxPreference) findPreference("daily_notification");
		listPreferenceDailyNotificationTimeValues = (ListPreference) findPreference("daily_notification_value");

		String gender = genderList.getValue();
		if (gender == null) {
			genderList.setValue((String) genderList.getEntryValues()[0]);
			gender = genderList.getValue();
		}
		genderList.setSummary(genderList.getEntries()[genderList
				.findIndexOfValue(gender)]);

		genderList
				.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
					@Override
					public boolean onPreferenceChange(Preference preference,
							Object newValue) {
						genderList.setSummary(genderList.getEntries()[genderList
								.findIndexOfValue(newValue.toString())]);
						return true;
					}
				});

		String height = heightList.getValue();
		if (height == null) {
			heightList.setValue((String) heightList.getEntryValues()[0]);
			height = heightList.getValue();
		}
		heightList.setSummary(heightList.getEntries()[heightList
				.findIndexOfValue(height)]);

		heightList
				.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
					@Override
					public boolean onPreferenceChange(Preference preference,
							Object newValue) {
						heightList.setSummary(heightList.getEntries()[heightList
								.findIndexOfValue(newValue.toString())]);
						return true;
					}
				});
		
		if(checkBoxPreferenceDaily.isChecked()) {
			listPreferenceDailyNotificationTimeValues.setEnabled(true);
		} else {
			listPreferenceDailyNotificationTimeValues.setEnabled(false);
		}
		
		
		checkBoxPreferenceDaily.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				if((Boolean)newValue) {
					listPreferenceDailyNotificationTimeValues.setEnabled(true);
				} else {
					listPreferenceDailyNotificationTimeValues.setEnabled(false);
				}
				return true;
			}
		});
		
//		String height = heightList.getValue();
//		if (height == null) {
//			heightList.setValue((String) heightList.getEntryValues()[0]);
//			height = heightList.getValue();
//		}
//		heightList.setSummary(heightList.getEntries()[heightList
//				.findIndexOfValue(height)]);
		
		
		String dailyNotificationTimeValue = listPreferenceDailyNotificationTimeValues.getValue();
		if(dailyNotificationTimeValue == null) {
			listPreferenceDailyNotificationTimeValues.setValue((String) listPreferenceDailyNotificationTimeValues.getEntryValues()[0]);
			dailyNotificationTimeValue = listPreferenceDailyNotificationTimeValues.getValue();
		}
		listPreferenceDailyNotificationTimeValues.setSummary(heightList.getEntries()[heightList.findIndexOfValue(dailyNotificationTimeValue)]);
		
		listPreferenceDailyNotificationTimeValues.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				return true;
			}
		});

	}
}
// ActionBar actionBar = getActionBar();
// actionBar.hide();
//
// initSettingsComponents();
// setValuesForHeightList();
//
// String gender = genderList.getValue();
// if (gender == null) {
// genderList.setValue((String) genderList.getEntryValues()[0]);
// gender = genderList.getValue();
// }
// genderList.setSummary(genderList.getEntries()[genderList
// .findIndexOfValue(gender)]);
//
// genderList
// .setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
// @Override
// public boolean onPreferenceChange(Preference preference,
// Object newValue) {
// genderList.setSummary(genderList.getEntries()[genderList
// .findIndexOfValue(newValue.toString())]);
// return true;
// }
// });
//
// // heightList.setDefaultValue(heightList.getEntryValues()[0]);
// String height = heightList.getValue();
// if (height == null) {
// heightList.setValue((String) heightList.getEntryValues()[0]);
// height = heightList.getValue();
// }
//
// Log.i("Height list value", height);
// try {
// heightList.setSummary(heightList.getEntries()[heightList
// .findIndexOfValue(height)]);
// } catch (Exception e) {
// heightList.setValue((String) heightList.getEntryValues()[height
// .length() / 2]);
// heightList.setSummary(heightList.getEntries()[height.length() / 2]);
// }
//
// heightList
// .setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
// @Override
// public boolean onPreferenceChange(Preference preference,
// Object newValue) {
// heightList.setSummary(heightList.getEntries()[heightList
// .findIndexOfValue(newValue.toString())]);
// return true;
// }
// });
//
// }
//
// protected String retriveFromSharedPreferences(String tag,
// String defaultValue) {
// SharedPreferences sharedPreferences = getSharedPreferences(
// getApplicationName(), MODE_PRIVATE);
// return sharedPreferences.getString(tag, defaultValue);
// }
//
// protected String getApplicationName() {
// final PackageManager pm = getApplicationContext().getPackageManager();
// ApplicationInfo ai;
// try {
// ai = pm.getApplicationInfo(this.getPackageName(), 0);
// } catch (final NameNotFoundException e) {
// ai = null;
// }
// final String applicationName = (String) (ai != null ? pm
// .getApplicationLabel(ai) : "(unknown)");
// return applicationName;
// }
//
// private void setValuesForHeightList() {
// String unitType = retriveFromSharedPreferences(UNITS, UNITS_METRIC);
// if (unitType.compareToIgnoreCase(UNITS_IMPERIAL) == 0) {
// heightList
// .setEntries(R.array.preference_choose_height_imperial_display);
// heightList
// .setEntryValues(R.array.preference_choose_height_imperial);
// heightList.setDefaultValue(getResources().getString(
// R.string.preference_height_default_value_imperial));
// }
//
// }
//
// @SuppressWarnings("deprecation")
// private void initSettingsComponents() {
// // unitsList = (ListPreference) findPreference("units_type");
// heightList = (ListPreference) findPreference("height");
// genderList = (ListPreference) findPreference("gender");
// }

