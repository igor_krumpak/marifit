package si.oranza.marifit.settings;

import si.oranza.marifit.R;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.Preference.OnPreferenceChangeListener;

public class SettingsImperialActivity extends PreferenceActivity {
	
	private ListPreference genderList;
	private ListPreference heightList;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preference_imperial);
		
		genderList = (ListPreference) findPreference("gender");
		heightList = (ListPreference) findPreference("height");

		genderList
				.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
					@Override
					public boolean onPreferenceChange(Preference preference,
							Object newValue) {
						genderList.setSummary(genderList.getEntries()[genderList
								.findIndexOfValue(newValue.toString())]);
						return true;
					}
				});

		heightList
				.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
					@Override
					public boolean onPreferenceChange(Preference preference,
							Object newValue) {
						heightList.setSummary(heightList.getEntries()[heightList
								.findIndexOfValue(newValue.toString())]);
						return true;
					}
				});
	}
}
