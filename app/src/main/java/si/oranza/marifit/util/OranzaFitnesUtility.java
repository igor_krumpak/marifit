package si.oranza.marifit.util;

import si.oranza.marifit.R;
import si.oranza.marifit.objects.BodyObject;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.TextView;

public class OranzaFitnesUtility {

	private static final double BMI_MALE_YELLOW_BORDER = 20.7;
	private static final double BMI_MALE_GREEN_BORDER = 26.4;
	private static final double BMI_MALE_ORANGE_BORDER = 31.1;
	private static final double BMI_FEMALE_YELLOW_BORDER = 19.1;
	private static final double BMI_FEMALE_GREEN_BORDER = 25.8;
	private static final double BMI_FEMALE_ORANGE_BORDER = 32.2;

	public static double calculateBMIMetric(double weight, double height) {
		Log.i("weight", weight + "");
		Log.i("height", height + "");
		return weight / (height * height);
	}


	public static void displayBMIOnTextView(SharedPreferences sharedPref,
			double weight, TextView textViewBMI, Context context) {
		double BMI = 0;
		if (sharedPref.getString(BodyObject.UNITS_KEY, BodyObject.METRIC)
				.equalsIgnoreCase(BodyObject.METRIC)) {
			BMI = OranzaFitnesUtility.calculateBMIMetric(weight, Double
					.parseDouble(sharedPref.getString(BodyObject.HEIGHT_KEY,
							BodyObject.HEIGHT_METRIC_DEFAULT_VALUE)) / 100);
		} else {
			BMI = OranzaFitnesUtility.calculateBMIImperial(weight, Double
					.parseDouble(sharedPref.getString(BodyObject.HEIGHT_KEY,
							BodyObject.HEIGHT_IMPERIAL_DEFAULT_VALUE)));
		}
		displyBMIByGender(sharedPref, BMI, textViewBMI, context);

	}

	private static void displyBMIByGender(SharedPreferences sharedPref,
			double BMI, TextView textViewBMI, Context context) {
		if (sharedPref.getString("gender", BodyObject.MALE).equalsIgnoreCase(
				BodyObject.MALE)) {
			textViewBMI.setTextColor(getMaleColors(BMI, context));
		} else {
			textViewBMI.setTextColor(getFemaleColors(BMI, context));
		}

		textViewBMI.setText(OranzaUtility.roundToOnePlace(BMI) + "");
	}

	private static int getFemaleColors(double BMI, Context context) {
		return getBMIColors(BMI, context, BMI_FEMALE_YELLOW_BORDER,
				BMI_FEMALE_GREEN_BORDER, BMI_FEMALE_ORANGE_BORDER);
	}

	private static int getMaleColors(double BMI, Context context) {
		return getBMIColors(BMI, context, BMI_MALE_YELLOW_BORDER,
				BMI_MALE_GREEN_BORDER, BMI_MALE_ORANGE_BORDER);
	}

	private static int getBMIColors(double BMI, Context context,
			double yelloBorder, double greenBorder, double orangeBorder) {
		if (BMI < yelloBorder)
			return context.getResources().getColor(R.color.yellow);
		else if (BMI >= yelloBorder && BMI <= greenBorder)
			return context.getResources().getColor(R.color.green);
		else if (BMI > greenBorder && BMI <= orangeBorder)
			return context.getResources().getColor(R.color.orange);
		return context.getResources().getColor(R.color.red);
	}

	public static double calculateBMI(double weight, double height) {
		Log.i("BMI METRIC", weight + " " + height);
		if(0.0 == weight) {
			return 0.0;
		}
		return OranzaUtility.roundToOnePlace(weight / (height/100 * height/100));
	}
	
	public static double calculateBMIImperial(double weight, double height) {
		Log.i("BMI IMPERIAL", weight + " " + height);
		if(0.0 == weight) {
			return 0.0;
		}
		return  OranzaUtility.roundToOnePlace((weight / (height * height)) * 703);
	}

}
