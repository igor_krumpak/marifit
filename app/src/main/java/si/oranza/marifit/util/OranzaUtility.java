package si.oranza.marifit.util;

import java.util.ArrayList;

import si.oranza.marifit.objects.BodyObject;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class OranzaUtility {

	public static final String takeSidePicture = "side_picture";
	public static final String takeFrontPicture = "front_picture";
	public static final String takeBackPicture = "back_picture";

	public static String getCurrentTimeInFormatedString() {
		return getTimeInFormatedString(getCurrentTime());
	}

	public static String getTimeInFormatedString(long millis) {
		Time t = new Time();
		t.set(millis);
		String minute = "";
		if (t.minute < 10) {
			minute = "0" + t.minute;
		} else {
			minute = "" + t.minute;
		}
		return t.hour + ":" + minute;
	}

	public static String getDateInFormatedString(long millis) {
		Time time = new Time();
		time.set(millis);
		return time.format("%d.%m.%Y");
	}

	public static long getCurrentTime() {
		return getCurrentTimeObject().toMillis(true);
	}

	public static OranzaDisplayData returnDisplaySize(Activity activity) {
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		OranzaDisplayData oranzaDisplayData = new OranzaDisplayData(
				metrics.heightPixels, metrics.widthPixels);
		return oranzaDisplayData;
	}

	public static float dipToPixels(float dip, Context context) {
		Resources r = context.getResources();
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip,
				r.getDisplayMetrics());
	}

	public static float pixelsToDip(float pixels, Context context) {
		DisplayMetrics displayMetrics = context.getResources()
				.getDisplayMetrics();
		return (int) ((pixels / displayMetrics.density) + 0.5);
	}

	public static double parseDouble(String string) {
		if (string.length() > 0)
			return Double.parseDouble(string);
		return 0;
	}

	public static String getCurrentDateInFormatedString() {
		return getCurrentTimeObject().format("%d.%m.%Y");
	}

	private static Time getCurrentTimeObject() {
		Time time = new Time();
		time.setToNow();
		return time;
	}

	public static int generateTimeAsID() {
		return Integer.parseInt(getCurrentTimeObject().format("%Y0%m0%d"));
	}

	public static ArrayList<String> getAllFrontPicturePaths(
			ArrayList<BodyObject> bodyObjects) {
		ArrayList<String> paths = new ArrayList<String>();
		for (BodyObject b : bodyObjects) {
			if (b.getFrontPicturePath().length() != 0) {
				paths.add(b.getFrontPicturePath());
			}
		}
		return paths;
	}

	public static ArrayList<String> getAllSidePicturePaths(
			ArrayList<BodyObject> bodyObjects) {
		ArrayList<String> paths = new ArrayList<String>();
		for (BodyObject b : bodyObjects) {
			if (b.getSidePicturePath().length() != 0) {
				paths.add(b.getSidePicturePath());
			}
		}

		return paths;
	}

	public static ArrayList<String> getAllBackPicturePaths(
			ArrayList<BodyObject> bodyObjects) {
		ArrayList<String> paths = new ArrayList<String>();
		for (BodyObject b : bodyObjects) {
			if (b.getBackPicturePath().length() != 0) {
				paths.add(b.getBackPicturePath());
			}
		}
		return paths;
	}

	public static double roundToOnePlace(double valueToRound) {
		return Math.floor(valueToRound * 10) / 10;
	}

}
