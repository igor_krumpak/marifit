package si.oranza.marifit.util;

import java.io.ByteArrayOutputStream;

import si.oranza.marifit.R;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

public class OranzaPictureUtility {

	public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 436321;

	public static Bitmap rotate(Bitmap b, int degrees) {
		Matrix matrix = new Matrix();
		matrix.postRotate(degrees);
		Bitmap rotated = Bitmap.createBitmap(b, 0, 0, b.getWidth(),
				b.getHeight(), matrix, false);
		return rotated;
	}

	public static int calculatePictureSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}
		Log.i("izra�unnVelikostSlike", inSampleSize + "");
		return inSampleSize;
	}

	// public static Options getRecomendedOptions(View cameraView, byte[] data)
	// {
	// final Options options = new BitmapFactory.Options();
	// options.inJustDecodeBounds = true;
	// BitmapFactory.decodeByteArray(data, 0, data.length, options);
	// Log.i("getRecomendedOptions", cameraView.getWidth() + " " +
	// cameraView.getHeight());
	// options.inSampleSize = OranzaUtility.izracunalVelikostSlike(options,
	// cameraView.getWidth(), cameraView.getHeight());
	// options.inJustDecodeBounds = false;
	// return options;
	// }

	public static Bitmap createScaledBitmap(Bitmap bitmap, int width, int height) {
		final Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inSampleSize = OranzaPictureUtility.calculatePictureSize(
				options, height, width);
		options.inJustDecodeBounds = false;
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height,
				false);
		bitmap.recycle();
		return scaledBitmap;
	}

	public static Bitmap loadBitmap(Uri imageFileUri, float width,
			float height, Context context) {
		Bitmap returnBmp = null;
		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inSampleSize = 32;
			returnBmp = BitmapFactory.decodeStream(context.getContentResolver()
					.openInputStream(imageFileUri), null, bmpFactoryOptions);
			returnBmp = Bitmap.createScaledBitmap(returnBmp, (int) width,
					(int) height, false);
		} catch (Exception e) {
			Log.v("ERROR", e.toString());
		}
		returnBmp = rotate(returnBmp, 90);
		return returnBmp;
	}

	public static byte[] changeBitmapToByteArray(Bitmap slika) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		slika.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		return baos.toByteArray();
	}

	public static Uri useBuildCamera(String prefix, Activity activity) {
		String fileName = prefix
				+ OranzaUtility.getCurrentTimeInFormatedString();
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		values.put(MediaStore.Images.Media.DESCRIPTION,
				"Image capture by camera");
		Uri imageUri = activity.getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		activity.startActivityForResult(intent,
				CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
		return imageUri;
	}

	public static String useCamera(Activity activity, String fileName,
			int requestCode) {
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		Uri imageUri = activity.getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		activity.startActivityForResult(intent, requestCode);
		String path = getRealPathFromURI(imageUri, activity);
		return path;
	}

	public static String getRealPathFromURI(Uri contentURI, Context context) {
		long millis = System.currentTimeMillis();
		Cursor cursor = context.getContentResolver().query(contentURI, null,
				null, null, null);
		cursor.moveToFirst();
		int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
		String realPath = "file:" + cursor.getString(idx);
		cursor.close();
		Log.i("TAG", "" + (System.currentTimeMillis() - millis));
		return realPath;
	}

	public static void loadBitmapOnImageView(String imageUrl,
			final ImageView imageView, Context context, int width, int height) {
		// DisplayImageOptions options = new DisplayImageOptions.Builder()
		// .showStubImage(R.drawable.ic_launcher)
		// .showImageForEmptyUri(R.drawable.ic_launcher)
		// .resetViewBeforeLoading().cacheInMemory().cacheOnDisc()
		// .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		// .bitmapConfig(Bitmap.Config.ARGB_8888).build();
		// ImageSize imageSize = new ImageSize(width, height);
		// ImageLoader.getInstance().loadImage(context, imageUrl, imageSize,
		// options, new ImageLoadingListener() {
		// @Override
		// public void onLoadingStarted() {
		// }
		//
		// @Override
		// public void onLoadingFailed(FailReason failReason) {
		// }
		//
		// @Override
		// public void onLoadingComplete(Bitmap loadedImage) {
		// imageView.setImageBitmap(rotate(loadedImage, 90));
		// }
		//
		// @Override
		// public void onLoadingCancelled() {
		// }
		// });
	}

	public static void loadBitmapOnImageView(String imageUrl,
			final ImageView imageView, Context context) {

	
		// DisplayImageOptions options = new DisplayImageOptions.Builder()
		// .showStubImage(R.drawable.ic_launcher)
		// .showImageForEmptyUri(R.drawable.ic_launcher)
		// .resetViewBeforeLoading().cacheInMemory().cacheOnDisc()
		// .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		// .bitmapConfig(Bitmap.Config.ARGB_8888).build();
		// ImageLoader.getInstance().loadImage(context, imageUrl, options,
		// new ImageLoadingListener() {
		// @Override
		// public void onLoadingStarted() {
		// }
		//
		// @Override
		// public void onLoadingFailed(FailReason failReason) {
		// }
		//
		// @Override
		// public void onLoadingComplete(Bitmap loadedImage) {
		// imageView.setImageBitmap(rotate(loadedImage, 90));
		// }
		//
		// @Override
		// public void onLoadingCancelled() {
		// }
		// });
	}

	public static void clearImageView(ImageView imageView) {
		imageView.setImageResource(R.drawable.ic_launcher);
	}

}
