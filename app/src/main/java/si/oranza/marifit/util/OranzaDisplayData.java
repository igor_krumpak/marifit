package si.oranza.marifit.util;

public class OranzaDisplayData {

	public float width;
	public float height;

	public OranzaDisplayData(float width, float height) {
		this.width = width;
		this.height = height;
	}

}
